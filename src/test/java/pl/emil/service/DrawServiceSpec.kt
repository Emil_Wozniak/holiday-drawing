package pl.emil.service

import io.kotest.core.spec.style.FeatureSpec
import io.kotest.matchers.shouldBe
import pl.emil.domain.Entrant

class DrawServiceSpec : FeatureSpec({

    val entrants = mutableListOf<Entrant>()
    beforeTest {
        (1..6).map { it.toLong() }.forEach {
            entrants.apply {
                add(Entrant(it, it + 2, 0L, 0L))
            }
        }
    }

    feature("Draw survive") {
        scenario("should assign drawn person to request user") {
            val results = (3..8).map { it.toLong() }.map { id ->
                var all = entrants
                var isNotMatching = true
                val userEntrant = all.find { entrant -> entrant.playerId == id }
                all
                    .shuffled()
                    .filter { it.personWhoDrawParticipantId == 0L }
                    .distinct()
                    .forEach { entrant ->
                        if (isNotMatching && entrant.playerId != id && entrant.personWhoDrawParticipantId == 0L) {
                            isNotMatching = false
                            userEntrant?.drawnPersonId = entrant.playerId
                            entrant.personWhoDrawParticipantId = userEntrant?.playerId
                            // test only save equivalent
                            all = all.map { next ->
                                if (next.playerId == id) next.drawnPersonId = entrant.playerId
                                if (next.playerId == entrant.playerId) next.personWhoDrawParticipantId = id
                                next
                            }.toMutableList()
                        }
                    }
                userEntrant
            }
            results.forEach { println(it) }
            val duplicates = results.groupingBy { it }.eachCount().filter { it.value > 1 }
            duplicates.values.size shouldBe 0
        }
    }
})
