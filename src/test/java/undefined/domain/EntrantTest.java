package pl.emil.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import pl.emil.web.rest.TestUtil;

class EntrantTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Entrant.class);
        Entrant entrant1 = new Entrant();
        entrant1.setId(1L);
        Entrant entrant2 = new Entrant();
        entrant2.setId(entrant1.getId());
        assertThat(entrant1).isEqualTo(entrant2);
        entrant2.setId(2L);
        assertThat(entrant1).isNotEqualTo(entrant2);
        entrant1.setId(null);
        assertThat(entrant1).isNotEqualTo(entrant2);
    }
}
