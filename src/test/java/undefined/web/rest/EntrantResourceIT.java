package pl.emil.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import pl.emil.IntegrationTest;
import pl.emil.domain.Entrant;
import pl.emil.repository.EntrantRepository;

/**
 * Integration tests for the {@link EntrantResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EntrantResourceIT {

    private static final Long DEFAULT_PLAYER_ID = 1L;
    private static final Long UPDATED_PLAYER_ID = 2L;

    private static final Long DEFAULT_DRAWN_PERSON_ID = 1L;
    private static final Long UPDATED_DRAWN_PERSON_ID = 2L;

    private static final Long DEFAULT_PERSON_WHO_DRAW_PARTICIPANT_ID = 1L;
    private static final Long UPDATED_PERSON_WHO_DRAW_PARTICIPANT_ID = 2L;

    private static final String ENTITY_API_URL = "/api/entrants";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EntrantRepository entrantRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEntrantMockMvc;

    private Entrant entrant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Entrant createEntity(EntityManager em) {
        Entrant entrant = new Entrant()
            .playerId(DEFAULT_PLAYER_ID)
            .drawnPersonId(DEFAULT_DRAWN_PERSON_ID)
            .personWhoDrawParticipantId(DEFAULT_PERSON_WHO_DRAW_PARTICIPANT_ID);
        return entrant;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Entrant createUpdatedEntity(EntityManager em) {
        Entrant entrant = new Entrant()
            .playerId(UPDATED_PLAYER_ID)
            .drawnPersonId(UPDATED_DRAWN_PERSON_ID)
            .personWhoDrawParticipantId(UPDATED_PERSON_WHO_DRAW_PARTICIPANT_ID);
        return entrant;
    }

    @BeforeEach
    public void initTest() {
        entrant = createEntity(em);
    }

    @Test
    @Transactional
    void createEntrant() throws Exception {
        int databaseSizeBeforeCreate = entrantRepository.findAll().size();
        // Create the Entrant
        restEntrantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entrant)))
            .andExpect(status().isCreated());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeCreate + 1);
        Entrant testEntrant = entrantList.get(entrantList.size() - 1);
        assertThat(testEntrant.getPlayerId()).isEqualTo(DEFAULT_PLAYER_ID);
        assertThat(testEntrant.getDrawnPersonId()).isEqualTo(DEFAULT_DRAWN_PERSON_ID);
        assertThat(testEntrant.getPersonWhoDrawParticipantId()).isEqualTo(DEFAULT_PERSON_WHO_DRAW_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    void createEntrantWithExistingId() throws Exception {
        // Create the Entrant with an existing ID
        entrant.setId(1L);

        int databaseSizeBeforeCreate = entrantRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEntrantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entrant)))
            .andExpect(status().isBadRequest());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkPlayerIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = entrantRepository.findAll().size();
        // set the field null
        entrant.setPlayerId(null);

        // Create the Entrant, which fails.

        restEntrantMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entrant)))
            .andExpect(status().isBadRequest());

        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEntrants() throws Exception {
        // Initialize the database
        entrantRepository.saveAndFlush(entrant);

        // Get all the entrantList
        restEntrantMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(entrant.getId().intValue())))
            .andExpect(jsonPath("$.[*].playerId").value(hasItem(DEFAULT_PLAYER_ID.intValue())))
            .andExpect(jsonPath("$.[*].drawnPersonId").value(hasItem(DEFAULT_DRAWN_PERSON_ID.intValue())))
            .andExpect(jsonPath("$.[*].personWhoDrawParticipantId").value(hasItem(DEFAULT_PERSON_WHO_DRAW_PARTICIPANT_ID.intValue())));
    }

    @Test
    @Transactional
    void getEntrant() throws Exception {
        // Initialize the database
        entrantRepository.saveAndFlush(entrant);

        // Get the entrant
        restEntrantMockMvc
            .perform(get(ENTITY_API_URL_ID, entrant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(entrant.getId().intValue()))
            .andExpect(jsonPath("$.playerId").value(DEFAULT_PLAYER_ID.intValue()))
            .andExpect(jsonPath("$.drawnPersonId").value(DEFAULT_DRAWN_PERSON_ID.intValue()))
            .andExpect(jsonPath("$.personWhoDrawParticipantId").value(DEFAULT_PERSON_WHO_DRAW_PARTICIPANT_ID.intValue()));
    }

    @Test
    @Transactional
    void getNonExistingEntrant() throws Exception {
        // Get the entrant
        restEntrantMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEntrant() throws Exception {
        // Initialize the database
        entrantRepository.saveAndFlush(entrant);

        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();

        // Update the entrant
        Entrant updatedEntrant = entrantRepository.findById(entrant.getId()).get();
        // Disconnect from session so that the updates on updatedEntrant are not directly saved in db
        em.detach(updatedEntrant);
        updatedEntrant
            .playerId(UPDATED_PLAYER_ID)
            .drawnPersonId(UPDATED_DRAWN_PERSON_ID)
            .personWhoDrawParticipantId(UPDATED_PERSON_WHO_DRAW_PARTICIPANT_ID);

        restEntrantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEntrant.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEntrant))
            )
            .andExpect(status().isOk());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
        Entrant testEntrant = entrantList.get(entrantList.size() - 1);
        assertThat(testEntrant.getPlayerId()).isEqualTo(UPDATED_PLAYER_ID);
        assertThat(testEntrant.getDrawnPersonId()).isEqualTo(UPDATED_DRAWN_PERSON_ID);
        assertThat(testEntrant.getPersonWhoDrawParticipantId()).isEqualTo(UPDATED_PERSON_WHO_DRAW_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    void putNonExistingEntrant() throws Exception {
        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();
        entrant.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEntrantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, entrant.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(entrant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEntrant() throws Exception {
        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();
        entrant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntrantMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(entrant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEntrant() throws Exception {
        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();
        entrant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntrantMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entrant)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEntrantWithPatch() throws Exception {
        // Initialize the database
        entrantRepository.saveAndFlush(entrant);

        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();

        // Update the entrant using partial update
        Entrant partialUpdatedEntrant = new Entrant();
        partialUpdatedEntrant.setId(entrant.getId());

        restEntrantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEntrant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEntrant))
            )
            .andExpect(status().isOk());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
        Entrant testEntrant = entrantList.get(entrantList.size() - 1);
        assertThat(testEntrant.getPlayerId()).isEqualTo(DEFAULT_PLAYER_ID);
        assertThat(testEntrant.getDrawnPersonId()).isEqualTo(DEFAULT_DRAWN_PERSON_ID);
        assertThat(testEntrant.getPersonWhoDrawParticipantId()).isEqualTo(DEFAULT_PERSON_WHO_DRAW_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    void fullUpdateEntrantWithPatch() throws Exception {
        // Initialize the database
        entrantRepository.saveAndFlush(entrant);

        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();

        // Update the entrant using partial update
        Entrant partialUpdatedEntrant = new Entrant();
        partialUpdatedEntrant.setId(entrant.getId());

        partialUpdatedEntrant
            .playerId(UPDATED_PLAYER_ID)
            .drawnPersonId(UPDATED_DRAWN_PERSON_ID)
            .personWhoDrawParticipantId(UPDATED_PERSON_WHO_DRAW_PARTICIPANT_ID);

        restEntrantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEntrant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEntrant))
            )
            .andExpect(status().isOk());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
        Entrant testEntrant = entrantList.get(entrantList.size() - 1);
        assertThat(testEntrant.getPlayerId()).isEqualTo(UPDATED_PLAYER_ID);
        assertThat(testEntrant.getDrawnPersonId()).isEqualTo(UPDATED_DRAWN_PERSON_ID);
        assertThat(testEntrant.getPersonWhoDrawParticipantId()).isEqualTo(UPDATED_PERSON_WHO_DRAW_PARTICIPANT_ID);
    }

    @Test
    @Transactional
    void patchNonExistingEntrant() throws Exception {
        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();
        entrant.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEntrantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, entrant.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(entrant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEntrant() throws Exception {
        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();
        entrant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntrantMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(entrant))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEntrant() throws Exception {
        int databaseSizeBeforeUpdate = entrantRepository.findAll().size();
        entrant.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntrantMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(entrant)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Entrant in the database
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEntrant() throws Exception {
        // Initialize the database
        entrantRepository.saveAndFlush(entrant);

        int databaseSizeBeforeDelete = entrantRepository.findAll().size();

        // Delete the entrant
        restEntrantMockMvc
            .perform(delete(ENTITY_API_URL_ID, entrant.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Entrant> entrantList = entrantRepository.findAll();
        assertThat(entrantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
