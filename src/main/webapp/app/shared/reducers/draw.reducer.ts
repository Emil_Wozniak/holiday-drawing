import axios from 'axios';
import { createAsyncThunk, createSlice, isFulfilled, isPending } from '@reduxjs/toolkit';
import { serializeAxiosError } from 'app/shared/reducers/reducer.utils';
import { IUser } from 'app/shared/model/user.model';

const initialState = {
  loading: false,
  errorMessage: null,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
  drawn: false,
  allDrawn: false,
  remain: 0,
  drawnPersonId: 0,
  drawnPerson: null as IUser,
};

const apiUrl = 'api/draw';

// Actions

export const drawPerson = createAsyncThunk(
  'draw/draw_person',
  async (id: number) => {
    const requestUrl = `${apiUrl}/${id}`;
    return await axios.post<IUser>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

export const hasDrawn = createAsyncThunk(
  'draw/has_drawn',
  async (id: number) => {
    const requestUrl = `${apiUrl}/${id}`;
    const res = axios.get<boolean>(requestUrl);
    return await res;
  },
  { serializeError: serializeAxiosError }
);

export const didAllDrawn = createAsyncThunk(
  'draw/did_all_drawn',
  async () => {
    const requestUrl = `${apiUrl}/all`;
    const res = axios.get<boolean>(requestUrl);
    return await res;
  },
  { serializeError: serializeAxiosError }
);

export const remainVotes = createAsyncThunk(
  'draw/remain_draws',
  async () => {
    const requestUrl = `${apiUrl}/remain`;
    const res = axios.get<number>(requestUrl);
    return await res;
  },
  { serializeError: serializeAxiosError }
);

export const getDrawnPerson = createAsyncThunk(
  'draw/get_drawn_person',
  async (id: number) => {
    const requestUrl = `${apiUrl}/${id}/person`;
    const res = axios.get<IUser>(requestUrl);
    return await res;
  },
  { serializeError: serializeAxiosError }
);

export type DrawnState = Readonly<typeof initialState>;

export const DrawnSlice = createSlice({
  name: 'drawn',
  initialState: initialState as DrawnState,
  reducers: {
    reset() {
      return initialState;
    },
  },
  extraReducers(builder) {
    builder
      .addCase(drawPerson.fulfilled, (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.drawn = !!action.payload;
        state.drawnPerson = action.payload.data;
        state.drawnPersonId = action.payload.data.id;
      })
      .addCase(getDrawnPerson.fulfilled, (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.drawnPerson = action.payload.data;
        state.drawnPersonId = action.payload.data.id;
      })
      .addCase(hasDrawn.fulfilled, (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.drawn = action.payload.data;
      })
      .addCase(didAllDrawn.fulfilled, (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.allDrawn = action.payload.data;
      })
      .addCase(remainVotes.fulfilled, (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.remain = action.payload.data;
      })
      .addMatcher(isFulfilled(drawPerson), (state, action) => {
        state.loading = false;
        state.updateSuccess = true;
        state.drawn = !!action.payload;
      })
      .addMatcher(isPending(drawPerson, hasDrawn, didAllDrawn, remainVotes, getDrawnPerson), state => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.loading = true;
      });
  },
});

export const { reset } = DrawnSlice.actions;

// Reducer
export default DrawnSlice.reducer;
