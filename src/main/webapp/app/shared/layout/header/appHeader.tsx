import './header.scss';

import React from 'react';
import { translate, Translate } from 'react-jhipster';
import { Menu } from 'antd';
import { Header } from 'antd/lib/layout/layout';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

const { Item } = Menu;

export interface IHeaderProps {
  isAuthenticated: boolean;
  isAdmin: boolean;
  ribbonEnv: string;
  isInProduction: boolean;
  isOpenAPIEnabled: boolean;
  currentLocale: string;
}

type ItemProp = {
  key: string;
  to: string;
  icon: any;
  contentKey: string;
};

const MenuItem: React.FC<ItemProp> = ({ key, to, icon, contentKey }) => (
  <Item key={key}>
    <Link to={to}>
      <Translate contentKey={contentKey} /> <FontAwesomeIcon icon={icon} />
    </Link>
  </Item>
);

const AppHeader = (props: IHeaderProps) => {
  const renderDevRibbon = () =>
    props.isInProduction === false ? (
      <div className="ribbon dev">
        <Translate contentKey={`global.ribbon.${props.ribbonEnv}`} />
      </div>
    ) : null;

  return (
    <Header>
      {renderDevRibbon()}
      <div className="logo">
        <Link to="/">{translate('global.title')}</Link>
      </div>
      <Menu theme="dark" mode="horizontal" className="d-flex align-items-center">
        {props.isAuthenticated && <MenuItem key="1" to="/" contentKey="global.menu.home" icon="home" />}
        {props.isAuthenticated && props.isAdmin && (
          <>
            <MenuItem icon="asterisk" key="2" to="/entrant" contentKey="global.menu.entities.entrant" />
            <MenuItem icon="asterisk" key="14" to="/player" contentKey="global.menu.entities.player" />
            <MenuItem key="3" to="/admin/user-management" contentKey="global.menu.admin.userManagement" icon="user" />
            <MenuItem key="4" to="/admin/metrics" icon="tachometer-alt" contentKey="global.menu.admin.metrics" />
            <MenuItem key="5" to="/admin/health" icon="heart" contentKey="global.menu.admin.health" />
            <MenuItem key="6" to="/admin/configuration" icon="cogs" contentKey="global.menu.admin.configuration" />
            <MenuItem key="7" to="/admin/logs" icon="tasks" contentKey="global.menu.admin.logs" />
            {props.isOpenAPIEnabled && <MenuItem key="8" to="/admin/docs" icon="book" contentKey="global.menu.admin.apidocs" />}
          </>
        )}
        {props.isAuthenticated ? (
          <>
            <MenuItem contentKey="global.menu.account.settings" key="9" to="/account/settings" icon="wrench" />
            <MenuItem key="10" contentKey="global.menu.account.password" to="/account/password" icon="lock" />
            <MenuItem key="11" to="/logout" contentKey="global.menu.account.logout" icon="sign-out-alt" />
          </>
        ) : (
          <>
            <MenuItem key="12" to="/login" contentKey="global.menu.account.login" icon="sign-in-alt" />
            <MenuItem key="13" contentKey="global.menu.account.register" to="/account/register" icon="user-plus" />
          </>
        )}
      </Menu>
    </Header>
  );
};

export default AppHeader;
