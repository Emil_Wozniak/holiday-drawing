import './footer.scss';

import React from 'react';
import { Footer } from 'antd/lib/layout/layout';
import { Card, Col, Row, Typography } from 'antd';
import { FacebookOutlined, LinkedinOutlined } from '@ant-design/icons';

const { Text, Title } = Typography;

const AppFooter = () => (
  <Footer>
    <Card style={{ height: '15vh' }}>
      <Row gutter={[8, 8]}>
        <Col span={8}>
          <Title style={{ margin: 0 }} level={5}>
            Kontakt
          </Title>
          <br />
          <Text>Telefon: 508522995</Text>
          <br />
          <Text>
            Email: <a href="mailto:emil.wozniak.591986@gmail.com">emil.wozniak.591986@gmail.com</a>
          </Text>
        </Col>
        <Col span={8}>
          <Title style={{ margin: 0 }} level={5}>
            Social media
          </Title>
          <a href="https://www.facebook.com/emil.wozniak.397" style={{ textDecoration: 'none' }}>
            <FacebookOutlined /> Mój profil na Facebooku
          </a>
          <br />
          <a href="https://www.linkedin.com/in/emil-wozniak-it/">
            <LinkedinOutlined /> Mój profil na LinkedIn
          </a>
        </Col>
        <Col span={8}>
          <Title style={{ margin: 0 }} level={5}>
            Licencja
          </Title>
          <Text>
            <a style={{ textDecoration: 'none' }} href="https://www.apache.org/licenses/LICENSE-2.0">
              Copyright: Apache 2.0
            </a>
          </Text>
        </Col>
      </Row>
    </Card>
  </Footer>
);

export default AppFooter;
