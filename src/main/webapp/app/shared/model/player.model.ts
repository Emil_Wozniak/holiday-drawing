export interface IPlayer {
  id?: number;
  playerId?: number;
  imageContentType?: string | null;
  image?: string | null;
  fullImage?: string | null;
}

export function imageToFullImage(player: IPlayer): string {
  return `data:${player.imageContentType};base64,${player.image}`;
}

export const defaultValue: Readonly<IPlayer> = {};
