export interface IEntrant {
  id?: number;
  playerId?: number;
  drawnPersonId?: number | null;
  personWhoDrawParticipantId?: number | null;
}

export const defaultValue: Readonly<IEntrant> = {};
