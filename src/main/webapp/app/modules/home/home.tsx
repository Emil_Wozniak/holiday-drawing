import './home.scss';

import React from 'react';
import { Link } from 'react-router-dom';
import { translate, Translate } from 'react-jhipster';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { Alert, Badge, Card, Col, Row, Typography } from 'antd';
import DrawList from 'app/components/draw/list/draw-list';
import { didAllDrawn, getDrawnPerson, remainVotes } from 'app/shared/reducers/draw.reducer';
import { IUser } from 'app/shared/model/user.model';
import DrawScreen from 'app/modules/draw/draw-screen';

const { Title } = Typography;

export const Home = () => {
  const [user, setUser] = React.useState(null as IUser);
  const account = useAppSelector(state => state.authentication.account);
  const dispatch = useAppDispatch();
  const didAll = useAppSelector(state => state.draw.allDrawn);
  const remain = useAppSelector(state => state.draw.remain);
  const drawnUser = useAppSelector(state => state.draw.drawnPerson);
  const drawn = useAppSelector(state => state.draw.drawn);
  const isUserLogin = () => account && account.login;

  React.useEffect(() => {
    dispatch(didAllDrawn());
    dispatch(remainVotes());
    if (account.id) {
      dispatch(getDrawnPerson(account.id));
    }
  }, []);

  React.useEffect(() => {
    if (drawn && user === null && account.id) {
      dispatch(getDrawnPerson(account.id));
      setUser(drawnUser);
    }
    if (drawn && user !== drawnUser && account.id) {
      setUser(drawnUser);
    }
  });

  return (
    <>
      <Badge.Ribbon
        style={{ zIndex: 100 }}
        color={isUserLogin() ? 'green' : 'volcano'}
        text={
          isUserLogin() ? (
            <Translate contentKey="home.logged.message" interpolate={{ username: account.login }}>
              You are logged in as user {account.login}.
            </Translate>
          ) : (
            <p>Musisz się zalogować.</p>
          )
        }
      />
      <Card>
        <div style={{ margin: '36px 0', textAlign: 'center' }}>
          <Title level={2}>{translate('home.title')}</Title>
        </div>
        {isUserLogin() ? (
          <Row justify="center" align="middle">
            {!didAll ? (
              <DrawScreen remain={remain} drawnUser={drawnUser} account={account} />
            ) : (
              <Col>
                <DrawList />
              </Col>
            )}
          </Row>
        ) : (
          <Alert
            type="warning"
            message={
              <>
                {translate('global.messages.info.authenticated.prefix')}{' '}
                <Link to="/login" className="alert-link">
                  <Translate contentKey="global.messages.info.authenticated.link">sign in</Translate>
                </Link>
              </>
            }
          />
        )}
      </Card>
    </>
  );
};

export default Home;
