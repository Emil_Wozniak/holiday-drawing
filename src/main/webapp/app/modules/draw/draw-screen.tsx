import React from 'react';
import DrawingButton from 'app/components/draw/button/drawing-button';
import { Col, Image, Row, Typography } from 'antd';
import { GiftOutlined, UserOutlined } from '@ant-design/icons';
import { IUser } from 'app/shared/model/user.model';

const { Title } = Typography;

type Props = {
  account: any;
  drawnUser: IUser;
  remain: number;
};

const DrawScreen: React.FC<Props> = ({ account, drawnUser, remain }) => {
  return (
    <Col>
      <div style={{ textAlign: 'center' }}>
        <GiftOutlined style={{ fontSize: 86 }} />
      </div>
      <br />
      <DrawingButton id={account.id} />
      <br />
      {drawnUser && (
        <div>
          <Row justify="center" align="middle">
            {drawnUser.image && drawnUser.imageType && drawnUser.imageType !== '' ? (
              <Image width={150} src={`data:${drawnUser.imageType};base64,${drawnUser.image}`} fallback="zdjęcie gracza" />
            ) : (
              <UserOutlined style={{ fontSize: 120 }} />
            )}
          </Row>
          <Title level={4} style={{ textAlign: 'center' }}>
            Osoba wylosowana dla Ciebie
            <br />
            {drawnUser.firstName} {drawnUser.lastName}
          </Title>
        </div>
      )}
      <br />
      <Title style={{ textAlign: 'center' }} level={5}>
        Pozostało {remain ? remain : 'nieznana ilość'} głosów.
      </Title>
    </Col>
  );
};

export default DrawScreen;
