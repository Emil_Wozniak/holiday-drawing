import React from 'react';
import { translate, ValidatedField } from 'react-jhipster';
import { Form } from 'reactstrap';
import { useForm } from 'react-hook-form';
import { Badge, Col, Modal, Row } from 'antd';

export interface ILoginModalProps {
  showModal: boolean;
  loginError: boolean;
  handleLogin: (username: string, password: string, rememberMe: boolean) => void;
  handleClose: () => void;
}

const LoginModal = (props: ILoginModalProps) => {
  const login = ({ username, password, rememberMe }) => {
    props.handleLogin(username, password, rememberMe);
  };

  const {
    handleSubmit,
    register,
    formState: { errors, touchedFields },
  } = useForm({ mode: 'onTouched' });

  const { loginError, handleClose } = props;

  return (
    <Modal
      title={translate('login.title')}
      visible={props.showModal}
      onCancel={handleClose}
      cancelText={translate('entity.action.cancel')}
      okText={translate('login.form.button')}
      onOk={handleSubmit(login)}
    >
      {loginError && <Badge.Ribbon color={'volcano'} text={translate('login.messages.error.authentication')} />}
      <Form onSubmit={handleSubmit(login)}>
        <Row>
          <Col style={loginError ? { marginTop: '4em' } : {}}>
            <Row gutter={[8, 8]}>
              <Col span={12}>
                <ValidatedField
                  name="username"
                  label={translate('global.form.username.label')}
                  placeholder={translate('global.form.username.placeholder')}
                  required
                  autoFocus
                  data-cy="username"
                  validate={{ required: 'Username cannot be empty!' }}
                  register={register}
                  error={errors.username}
                  isTouched={touchedFields.username}
                />
              </Col>
              <Col span={12}>
                <ValidatedField
                  name="password"
                  type="password"
                  label={translate('login.form.password')}
                  placeholder={translate('login.form.password.placeholder')}
                  required
                  data-cy="password"
                  validate={{ required: 'Password cannot be empty!' }}
                  register={register}
                  error={errors.password}
                  isTouched={touchedFields.password}
                />
              </Col>
            </Row>
            <ValidatedField
              name="rememberMe"
              type="checkbox"
              check
              label={translate('login.form.rememberme')}
              value={true}
              register={register}
            />
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};

export default LoginModal;
