import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { getSortState, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { getUsersAsAdmin } from './user-management.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { Button, Table, Tag } from 'antd';
import { IUser } from 'app/shared/model/user.model';

export const UserManagement = (props: RouteComponentProps<any>) => {
  const dispatch = useAppDispatch();

  const [pagination, setPagination] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );

  const getUsersFromProps = () => {
    dispatch(
      getUsersAsAdmin({
        page: pagination.activePage - 1,
        size: pagination.itemsPerPage,
        sort: `${pagination.sort},${pagination.order}`,
      })
    );
    const endURL = `?page=${pagination.activePage}&sort=${pagination.sort},${pagination.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    getUsersFromProps();
  }, [pagination.activePage, pagination.order, pagination.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sortParam = params.get(SORT);
    if (page && sortParam) {
      const sortSplit = sortParam.split(',');
      setPagination({
        ...pagination,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () =>
    setPagination({
      ...pagination,
      order: pagination.order === ASC ? DESC : ASC,
      sort: p,
    });

  const handleSyncList = () => {
    getUsersFromProps();
  };

  const mapToDataTable = (users: IUser[]) => ({
    columns: [
      {
        title: translate('global.field.id'),
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => (
          <Button color="link" size="small" style={{ padding: 0, border: 0 }}>
            <Tag style={{ width: '100%' }}>
              <Link to={`${match.url}/${text}`}>{text}</Link>
            </Tag>
          </Button>
        ),
      },
      {
        title: translate('userManagement.login'),
        dataIndex: 'login',
        key: 'login',
        filters: users.map(it => ({ text: it.login, value: it.login })),
        onFilter: (value, record) => record.login.indexOf(value) === 0,
        sorter: (a, b) => (a > b ? 1 : -1),
        onClick: () => sort('login'),
      },
      {
        title: translate('userManagement.email'),
        dataIndex: 'email',
        key: 'email',
        onFilter: (value, record) => record.email.indexOf(value) === 0,
        filters: users.map(it => ({ text: it.email, value: it.email })),
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => <div>{text}</div>,
      },
      {
        title: translate('userManagement.langKey'),
        dataIndex: 'langKey',
        key: 'langKey',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => <div>{text}</div>,
      },
      {
        title: translate('userManagement.profiles'),
        dataIndex: 'authorities',
        key: 'authorities',
        render: (authorities: any[]) =>
          authorities
            ? authorities.map((authority, j) => (
                <div key={`user-auth-${authority.id}-${j}`}>
                  <Tag color="cyan">{authority.replace('ROLE_', '')}</Tag>
                </div>
              ))
            : null,
      },
      {
        title: translate('userManagement.lastModifiedBy'),
        dataIndex: 'lastModifiedBy',
        key: 'lastModifiedBy',
        onClick: () => sort('lastModifiedBy'),
        render: text => <div>{text}</div>,
      },
      {
        title: translate('userManagement.lastModifiedDate'),
        dataIndex: 'lastModifiedDate',
        key: 'lastModifiedDate',
        onClick: () => sort('lastModifiedDate'),
        render: text => <div>{text}</div>,
      },
      {
        title: '',
        dataIndex: 'login',
        key: 'login',
        onClick: () => sort('login'),
        render: text => (
          <div className="btn-group flex-btn-group-container">
            <Button size="small" style={{ padding: 0, border: 0 }}>
              <Tag color="blue" icon={<FontAwesomeIcon icon="eye" />} style={{ padding: 0 }}>
                <Link to={`${match.url}/${text}`}>{translate('entity.action.view')}</Link>
              </Tag>
            </Button>
            <Button style={{ padding: 0, border: 0 }} size="small">
              <Tag color="gold" icon={<FontAwesomeIcon icon="pencil-alt" />} style={{ padding: 0 }}>
                <Link to={`${match.url}/${text}/edit`}>{translate('entity.action.edit')}</Link>
              </Tag>
            </Button>
            <Button size="small" style={{ padding: 0, border: 0 }} disabled={account.login === text}>
              <Tag color="volcano" icon={<FontAwesomeIcon icon="trash" />} style={{ padding: 0 }}>
                <Link to={`${match.url}/${text}/delete`}>{translate('entity.action.delete')}</Link>
              </Tag>
            </Button>
          </div>
        ),
      },
    ],
    data: users,
  });

  const { match } = props;
  const account = useAppSelector(state => state.authentication.account);
  const users = useAppSelector(state => state.userManagement.users);
  const loading = useAppSelector(state => state.userManagement.loading);

  return (
    <>
      <h2 id="user-management-page-heading" data-cy="userManagementPageHeading">
        {translate('userManagement.home.title')}
        <div className="d-flex justify-content-end">
          <Button
            type="text"
            shape="circle"
            icon={<FontAwesomeIcon icon="sync" spin={loading} />}
            className="mr-2"
            onClick={handleSyncList}
            disabled={loading}
          />
          <Button type="primary" shape="circle" icon={<FontAwesomeIcon icon="plus" />}>
            <Link to={`${match.url}/new`} />
          </Button>
        </div>
      </h2>
      <Table columns={mapToDataTable(users.concat()).columns} dataSource={mapToDataTable(users.concat()).data} />
    </>
  );
};

export default UserManagement;
