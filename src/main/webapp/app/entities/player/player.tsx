import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { translate, Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntities } from './player.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { Button, Image, Table, Typography } from 'antd';

const { Title } = Typography;

export const Player = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();
  const playerList = useAppSelector(state => state.player.entities);
  const loading = useAppSelector(state => state.player.loading);

  useEffect(() => {
    dispatch(getEntities({}));
  }, []);

  const handleSyncList = () => {
    dispatch(getEntities({}));
  };

  const { match } = props;

  const mapToDataTable = () => ({
    columns: [
      {
        title: translate('global.field.id'),
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => (
          <Button size="small" style={{ padding: 0, border: 0 }}>
            <Link to={`${match.url}/${text}`}>
              <Title level={5}>{text}</Title>
            </Link>
          </Button>
        ),
      },
      {
        title: translate('holidayDrawingApp.player.playerId'),
        dataIndex: 'playerId',
        key: 'playerId',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => <Title level={5}>{text}</Title>,
      },
      {
        title: translate('holidayDrawingApp.player.image'),
        dataIndex: 'fullImage',
        key: 'fullImage',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: image => (
          <div>
            <Image width={60} src={image} fallback="zdjęcie gracza" />
          </div>
        ),
      },
    ],
    data: playerList,
  });

  return (
    <div>
      <Title level={2} id="user-management-page-heading" data-cy="userManagementPageHeading">
        {translate('userManagement.home.title')}
        <div className="d-flex justify-content-end">
          <Button
            type="text"
            shape="circle"
            icon={<FontAwesomeIcon icon="sync" spin={loading} />}
            className="mr-2"
            onClick={handleSyncList}
            disabled={loading}
          />
          <Button type="primary" shape="circle" icon={<FontAwesomeIcon icon="plus" />}>
            <Link to={`${match.url}/new`} />
          </Button>
        </div>
      </Title>

      <div>
        {playerList && playerList.length > 0 ? (
          <Table columns={mapToDataTable().columns} dataSource={mapToDataTable().data} />
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="holidayDrawingApp.player.home.notFound">No Players found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default Player;
