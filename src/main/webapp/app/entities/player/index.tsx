import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Player from './player';
import PlayerDetail from './player-detail';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PlayerDetail} />
      <ErrorBoundaryRoute path={match.url} component={Player} />
    </Switch>
  </>
);

export default Routes;
