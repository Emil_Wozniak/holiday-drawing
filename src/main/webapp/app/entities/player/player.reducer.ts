import axios from 'axios';
import { createAsyncThunk, isFulfilled, isPending } from '@reduxjs/toolkit';
import { createEntitySlice, EntityState, IQueryParams, serializeAxiosError } from 'app/shared/reducers/reducer.utils';
import { defaultValue, imageToFullImage, IPlayer } from 'app/shared/model/player.model';

const initialState: EntityState<IPlayer> = {
  loading: false,
  errorMessage: null,
  entities: [],
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

const apiUrl = 'api/players';

// Actions

export const getEntities = createAsyncThunk('player/fetch_entity_list', async ({ page, size, sort }: IQueryParams) => {
  const requestUrl = `${apiUrl}?cacheBuster=${new Date().getTime()}`;
  return axios.get<IPlayer[]>(requestUrl);
});

export const getEntity = createAsyncThunk(
  'player/fetch_entity',
  async (id: string | number) => {
    const requestUrl = `${apiUrl}/${id}`;
    return axios.get<IPlayer>(requestUrl);
  },
  { serializeError: serializeAxiosError }
);

// slice

export const PlayerSlice = createEntitySlice({
  name: 'player',
  initialState,
  extraReducers(builder) {
    builder
      .addCase(getEntity.fulfilled, (state, action) => {
        const player = action.payload.data;
        if (player.image) {
          player.fullImage = imageToFullImage(player);
        }
        state.loading = false;
        state.entity = player;
      })
      .addMatcher(isFulfilled(getEntities), (state, action) => {
        const players: IPlayer[] = action.payload.data;
        players.forEach(player => {
          if (player.image) {
            player.fullImage = imageToFullImage(player);
          }
        });
        return {
          ...state,
          loading: false,
          entities: players,
        };
      })
      .addMatcher(isPending(getEntities, getEntity), state => {
        state.errorMessage = null;
        state.updateSuccess = false;
        state.loading = true;
      });
  },
});

export const { reset } = PlayerSlice.actions;

// Reducer
export default PlayerSlice.reducer;
