import { IUser } from 'app/shared/model/user.model';

export type UserCard = {
  playerId: any;
  firstName: string;
  lastName: string;
  drawnPerson: IUser;
  personWhoDrawParticipant: IUser;
  hasBeenDrawn: boolean;
  hasDrawParticipant: boolean;
};
