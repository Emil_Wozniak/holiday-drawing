import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Entrant from './entrant';
import Player from './player';

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}entrant`} component={Entrant} />
      <ErrorBoundaryRoute path={`${match.url}player`} component={Player} />
    </Switch>
  </div>
);

export default Routes;
