import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { getSortState, JhiItemCount, JhiPagination, Translate, translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntities } from './entrant.reducer';
import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'app/shared/util/pagination.constants';
import { overridePaginationStateWithQueryParams } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { Button, Table, Typography } from 'antd';

const { Text, Title } = Typography;

export const Entrant = (props: RouteComponentProps<{ url: string }>) => {
  const dispatch = useAppDispatch();

  const [paginationState, setPaginationState] = useState(
    overridePaginationStateWithQueryParams(getSortState(props.location, ITEMS_PER_PAGE, 'id'), props.location.search)
  );

  const entrantList = useAppSelector(state => state.entrant.entities);
  const loading = useAppSelector(state => state.entrant.loading);

  const getAllEntities = () => {
    dispatch(
      getEntities({
        page: paginationState.activePage - 1,
        size: paginationState.itemsPerPage,
        sort: `${paginationState.sort},${paginationState.order}`,
      })
    );
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?page=${paginationState.activePage}&sort=${paginationState.sort},${paginationState.order}`;
    if (props.location.search !== endURL) {
      props.history.push(`${props.location.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [paginationState.activePage, paginationState.order, paginationState.sort]);

  useEffect(() => {
    const params = new URLSearchParams(props.location.search);
    const page = params.get('page');
    const sort = params.get(SORT);
    if (page && sort) {
      const sortSplit = sort.split(',');
      setPaginationState({
        ...paginationState,
        activePage: +page,
        sort: sortSplit[0],
        order: sortSplit[1],
      });
    }
  }, [props.location.search]);

  const sort = p => () => {
    setPaginationState({
      ...paginationState,
      order: paginationState.order === ASC ? DESC : ASC,
      sort: p,
    });
  };

  const handlePagination = currentPage =>
    setPaginationState({
      ...paginationState,
      activePage: currentPage,
    });

  const handleSyncList = () => {
    sortEntities();
  };

  const { match } = props;

  const mapToDataTable = () => ({
    columns: [
      {
        title: translate('global.field.id'),
        dataIndex: 'id',
        key: 'id',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => (
          <Button size="small" style={{ padding: 0, border: 0 }}>
            <Link to={`${match.url}/${text}`}>
              <Text>{text}</Text>
            </Link>
          </Button>
        ),
      },
      {
        title: translate('drawingApp.entrant.id'),
        dataIndex: 'playerId',
        key: 'playerId',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => <Text>{text}</Text>,
      },
      {
        title: translate('drawingApp.entrant.playerId'),
        dataIndex: 'drawnPersonId',
        key: 'drawnPersonId',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => <Text>{text}</Text>,
      },
      {
        title: translate('drawingApp.entrant.personWhoDrawParticipantId'),
        dataIndex: 'personWhoDrawParticipantId',
        key: 'personWhoDrawParticipantId',
        sorter: (a, b) => (a > b ? 1 : -1),
        render: text => <Text>{text}</Text>,
      },
    ],
    data: entrantList,
  });

  return (
    <div>
      <Title level={2} id="user-management-page-heading" data-cy="userManagementPageHeading">
        {translate('userManagement.home.title')}
        <div className="d-flex justify-content-end">
          <Button
            type="text"
            shape="circle"
            icon={<FontAwesomeIcon icon="sync" spin={loading} />}
            className="mr-2"
            onClick={handleSyncList}
            disabled={loading}
          />
          <Button type="primary" shape="circle" icon={<FontAwesomeIcon icon="plus" />}>
            <Link to={`${match.url}/new`} />
          </Button>
        </div>
      </Title>
      <div className="table-responsive">
        {entrantList && entrantList.length > 0 ? (
          <Table columns={mapToDataTable().columns} dataSource={mapToDataTable().data} />
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="drawingApp.entrant.home.notFound">No Entrants found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default Entrant;
