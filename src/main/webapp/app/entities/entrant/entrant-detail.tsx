import React, { useEffect } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { getEntity } from './entrant.reducer';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const EntrantDetail = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(getEntity(props.match.params.id));
  }, []);

  const entrantEntity = useAppSelector(state => state.entrant.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="entrantDetailsHeading">
          <Translate contentKey="drawingApp.entrant.detail.title">Entrant</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{entrantEntity.id}</dd>
          <dt>
            <span id="playerId">
              <Translate contentKey="drawingApp.entrant.drwalerId">Drwaler Id</Translate>
            </span>
          </dt>
          <dd>{entrantEntity.playerId}</dd>
          <dt>
            <span id="drawnPersonId">
              <Translate contentKey="drawingApp.entrant.drawnPersonId">Drawn Person Id</Translate>
            </span>
          </dt>
          <dd>{entrantEntity.drawnPersonId}</dd>
          <dt>
            <span id="personWhoDrawParticipantId">
              <Translate contentKey="drawingApp.entrant.personWhoDrawParticipantId">Person Who Draw Participant Id</Translate>
            </span>
          </dt>
          <dd>{entrantEntity.personWhoDrawParticipantId}</dd>
        </dl>
        <Button tag={Link} to="/entrant" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/entrant/${entrantEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default EntrantDetail;
