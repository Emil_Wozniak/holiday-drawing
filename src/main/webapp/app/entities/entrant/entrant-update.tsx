import React, { useEffect, useState } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { createEntity, getEntity, reset, updateEntity } from './entrant.reducer';
import { useAppDispatch, useAppSelector } from 'app/config/store';

export const EntrantUpdate = (props: RouteComponentProps<{ id: string }>) => {
  const dispatch = useAppDispatch();

  const [isNew] = useState(!props.match.params || !props.match.params.id);

  const entrantEntity = useAppSelector(state => state.entrant.entity);
  const loading = useAppSelector(state => state.entrant.loading);
  const updating = useAppSelector(state => state.entrant.updating);
  const updateSuccess = useAppSelector(state => state.entrant.updateSuccess);

  const handleClose = () => {
    props.history.push('/entrant' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(props.match.params.id));
    }
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  const saveEntity = values => {
    const entity = {
      ...entrantEntity,
      ...values,
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {}
      : {
          ...entrantEntity,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="drawingApp.entrant.home.createOrEditLabel" data-cy="EntrantCreateUpdateHeading">
            <Translate contentKey="drawingApp.entrant.home.createOrEditLabel">Create or edit a Entrant</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="entrant-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('drawingApp.entrant.drwalerId')}
                id="entrant-drwalerId"
                name="drwalerId"
                data-cy="drwalerId"
                type="text"
                validate={{
                  required: { value: true, message: translate('entity.validation.required') },
                  validate: v => isNumber(v) || translate('entity.validation.number'),
                }}
              />
              <ValidatedField
                label={translate('drawingApp.entrant.drawnPersonId')}
                id="entrant-drawnPersonId"
                name="drawnPersonId"
                data-cy="drawnPersonId"
                type="text"
              />
              <ValidatedField
                label={translate('drawingApp.entrant.personWhoDrawParticipantId')}
                id="entrant-personWhoDrawParticipantId"
                name="personWhoDrawParticipantId"
                data-cy="personWhoDrawParticipantId"
                type="text"
              />
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/entrant" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default EntrantUpdate;
