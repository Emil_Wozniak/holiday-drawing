import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Entrant from './entrant';
import EntrantDetail from './entrant-detail';
import EntrantUpdate from './entrant-update';
import EntrantDeleteDialog from './entrant-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={EntrantUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={EntrantUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={EntrantDetail} />
      <ErrorBoundaryRoute path={match.url} component={Entrant} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={EntrantDeleteDialog} />
  </>
);

export default Routes;
