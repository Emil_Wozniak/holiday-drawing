import React, { useEffect, useState } from 'react';
import ListModal from 'app/components/draw/list-modal';
import { Badge, Button, Card, Col, Divider, Row, Typography } from 'antd';
import { UserOutlined } from '@ant-design/icons/lib/icons';
import { UserCard } from 'app/entities/user-card';

const { Title } = Typography;
const { Meta } = Card;

type CardProps = {
  card: UserCard;
};
const PlayerCard: React.FC<CardProps> = ({ card }) => {
  const [state, setState] = useState(null as UserCard);
  useEffect(() => {
    if (!state) {
      setState(card);
    }
    if (state !== card) {
      setState(card);
    }
  });
  return state ? (
    <Col className="gutter-row" span={8}>
      <Card hoverable cover={<UserOutlined twoToneColor="#eb2f96" style={{ fontSize: 120, marginTop: 8 }} />}>
        <Meta
          title={
            <Row justify="center" style={{ marginTop: 8 }}>
              <Title level={4} style={{ textAlign: 'center' }}>
                Gracz nr. {state.playerId}. {state.firstName} {state.lastName}
              </Title>
              <Row>
                <Badge style={{ backgroundColor: 'green' }} dot={state.hasBeenDrawn} />
                <Badge style={{ backgroundColor: 'rgb(245, 106, 0)' }} dot={state.personWhoDrawParticipant !== null} />
                <Badge style={{ backgroundColor: 'rgb(114, 101, 230)' }} dot={state.drawnPerson !== null} />
              </Row>
            </Row>
          }
          description={
            <Row justify="center">
              <Divider orientation="left">
                <Button shape="round" size="small" type="dashed">
                  Wylosuj osobę
                </Button>
              </Divider>
              <ListModal personWhoDrawParticipant={state.personWhoDrawParticipant} drawnPerson={state.drawnPerson} />
            </Row>
          }
        />
      </Card>
    </Col>
  ) : (
    <div />
  );
};

export default PlayerCard;
