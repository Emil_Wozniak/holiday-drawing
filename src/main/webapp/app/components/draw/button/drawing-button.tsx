import React from 'react';
import { Button, Typography } from 'antd';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { drawPerson, hasDrawn } from 'app/shared/reducers/draw.reducer';

const { Title } = Typography;

type Props = {
  id: number;
};

const DrawingButton: React.FC<Props> = ({ id }) => {
  const dispatch = useAppDispatch();
  const drawn = useAppSelector(state => state.draw.drawn);
  React.useEffect(() => {
    dispatch(hasDrawn(id));
  }, []);
  return (
    <div style={{ textAlign: 'center' }}>
      <Button
        type="primary"
        shape="round"
        key="draw-person"
        style={drawn ? { background: 'green', width: '100%' } : { width: '100%' }}
        onClick={() => {
          dispatch(drawPerson(id));
        }}
      >
        Wylosuj kogoś.
      </Button>
      {drawn && <Title level={5}>Oczekuj na głosy innych</Title>}
    </div>
  );
};

export default DrawingButton;
