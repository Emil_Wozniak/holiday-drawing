import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from 'app/config/store';
import { getEntities } from 'app/entities/entrant/entrant.reducer';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { Badge, Button, Divider, Row, Spin } from 'antd';
import { IUser } from 'app/shared/model/user.model';
import PlayerCard from 'app/components/draw/card/PlayerCard';
import { IEntrant } from 'app/shared/model/entrant.model';
import { UserCard } from 'app/entities/user-card';

const DrawList = () => {
  const [players, setPlayers] = useState([] as IUser[]);
  const [los, setLos] = useState([] as IEntrant[]);
  const dispatch = useAppDispatch();
  const entrants = useAppSelector(state => {
    const entities = state.entrant.entities;
    if (entities.length > 0 && entities.map(e => los.includes(e)).includes(false)) {
      setLos(entities.concat());
    }
    return entities;
  });
  const users = useAppSelector(state => {
    const entities = state.userManagement.users;
    if (entities.length > 0 && entities.map(e => players.includes(e)).includes(false)) {
      setPlayers(entities.concat());
    }
    return entities;
  });
  const [cards, setCards] = useState([]);
  const [isLoading, setLoad] = useState(false);

  const getCards = () => {
    setCards([]);
    const transformed = players
      .filter(user => user.login !== 'user' && user.login !== 'admin')
      .map(user => {
        const match = los.find(entrant => entrant.playerId === user.id);
        const getParticipant = (id: number) => (id !== 0 ? players.find(it => it.id === id) : null);
        if (match) {
          return {
            playerId: match.id,
            firstName: user.firstName,
            lastName: user.lastName,
            drawnPerson: getParticipant(match.drawnPersonId),
            personWhoDrawParticipant: getParticipant(match.personWhoDrawParticipantId),
            hasBeenDrawn: los.find(it => it.drawnPersonId === user.id) !== null,
            hasDrawParticipant: los.find(it => it.personWhoDrawParticipantId === user.id),
          } as UserCard;
        }
      })
      .filter(it => it);
    setCards(transformed);
  };

  const findEntities = async () => {
    await getAllEntrants();
    await getAllUsers();
  };
  useEffect(() => {
    if (!entrants.map(e => los.includes(e)).includes(true)) {
      getCards();
    }
  });

  useEffect(() => {
    findEntities().then(() => {
      setLoad(true);
      getCards();
      setLoad(false);
    });
  }, []);

  const getAllUsers = async () => {
    await dispatch(
      getUsers({
        page: 0,
        size: 20,
        sort: '',
      })
    );
  };
  const getAllEntrants = async () => {
    await dispatch(
      getEntities({
        page: 0,
        size: 20,
        sort: '',
      })
    );
  };
  return (
    <>
      <Button
        type="primary"
        shape="round"
        key="get-all-entities"
        onClick={() => {
          findEntities().then(() => {
            setLoad(true);
            getCards();
            setLoad(false);
          });
        }}
      >
        Pobierz listę
      </Button>
      {isLoading && <Spin style={{ marginLeft: '1em' }} />}
      <hr />
      {cards && cards.length > 0 && (
        <>
          <Divider orientation="left">Wybrani Gracze</Divider>
          <Row>
            <Badge text="ktoś został wylosowany" style={{ backgroundColor: 'green' }} dot />
            <Badge text="ktoś został wylosowany dla gracza" style={{ backgroundColor: 'rgb(245, 106, 0)' }} dot />
            <Badge text="ktoś wylosował gracza" style={{ backgroundColor: 'rgb(114, 101, 230)' }} dot />
          </Row>
          <Row gutter={[8, 8]}>
            {cards.map((card, id) => (
              <PlayerCard key={`entrant-card-${card.playerId}-${id}`} card={card} />
            ))}
          </Row>
        </>
      )}
    </>
  );
};

export default DrawList;
