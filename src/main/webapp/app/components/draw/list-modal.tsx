import React, { useState } from 'react';
import { Avatar, Button, Divider, Modal, Row, Typography } from 'antd';
import { IUser } from 'app/shared/model/user.model';
import { UserOutlined } from '@ant-design/icons';

const { Title } = Typography;

type Props = {
  drawnPerson: IUser;
  personWhoDrawParticipant: IUser;
};
const ListModal: React.FC<Props> = ({ drawnPerson, personWhoDrawParticipant }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const colors = ['#f56a00', '#7265e6'];
  return (
    <div>
      <Button disabled={drawnPerson == null && personWhoDrawParticipant === null} type="primary" onClick={showModal}>
        Gracze
      </Button>
      <Modal title="Wyniki" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <>
          <br />
          {drawnPerson && (
            <Row>
              <Row style={{ width: '100%' }}>
                <Avatar shape="square" size="large" style={{ backgroundColor: colors[0], marginLeft: 4 }} icon={<UserOutlined />} />
                <Title level={4} style={{ fontSize: 24, marginLeft: 4 }}>
                  <span style={{ fontWeight: 'normal' }}>Gracz </span>
                  {drawnPerson.firstName} {drawnPerson.lastName}
                </Title>
              </Row>
              <Title level={5} style={{ marginTop: 8 }}>
                Został wylosowany dla gracza
              </Title>
            </Row>
          )}
          <Divider />
          {personWhoDrawParticipant && (
            <Row>
              <Row style={{ width: '100%' }}>
                <Avatar shape="square" size="large" style={{ backgroundColor: colors[1], marginLeft: 4 }} icon={<UserOutlined />} />
                <Title level={4} style={{ fontSize: 24, marginLeft: 4 }}>
                  <span style={{ fontWeight: 'normal' }}>Gracz </span>
                  {personWhoDrawParticipant.firstName} {personWhoDrawParticipant.lastName}
                </Title>
              </Row>
              <Title level={5} style={{ marginTop: 8 }}>
                Wylosował gracza
              </Title>
            </Row>
          )}
          <br />
        </>
      </Modal>
    </div>
  );
};

export default ListModal;
