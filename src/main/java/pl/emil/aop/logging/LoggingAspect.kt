package pl.emil.aop.logging

import org.aspectj.lang.JoinPoint
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.AfterThrowing
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Pointcut
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles.of
import tech.jhipster.config.JHipsterConstants.SPRING_PROFILE_DEVELOPMENT
import java.util.*

/**
 * Aspect for logging execution of service and repository Spring components.
 *
 * By default, it only runs with the "dev" profile.
 */
@Aspect
class LoggingAspect(private val env: Environment) {
    /**
     * Pointcut that matches all repositories, services and Web REST endpoints.
     */
    @Pointcut("within(@org.springframework.stereotype.Repository *)" +
        " || within(@org.springframework.stereotype.Service *)" +
        " || within(@org.springframework.web.bind.annotation.RestController *)")
    fun springBeanPointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Pointcut that matches all Spring beans in the application's main packages.
     */
    @Pointcut("within(pl.emil.repository..*)" + " || within(pl.emil.service..*)" + " || within(pl.emil.web.rest..*)")
    fun applicationPackagePointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    /**
     * Retrieves the [Logger] associated to the given [JoinPoint].
     *
     * @param joinPoint join point we want the logger for.
     * @return [Logger] associated to the given [JoinPoint].
     */
    private fun logger(joinPoint: JoinPoint): Logger = LoggerFactory.getLogger(joinPoint.signature.declaringTypeName)

    /**
     * Advice that logs methods throwing exceptions.
     *
     * @param joinPoint join point for advice.
     * @param e exception.
     */
    @AfterThrowing(pointcut = "applicationPackagePointcut() && springBeanPointcut()", throwing = "e")
    fun logAfterThrowing(joinPoint: JoinPoint, e: Throwable) {
        with(logger(joinPoint)) {
            when {
                isDev() -> error("Exception in ${joinPoint.signature.name}() with cause = '${if (e.cause != null) e.cause else "NULL"}' and exception = '${e.message}'")
                else -> error("Exception in ${joinPoint.signature.name}() with cause = ${if (e.cause != null) e.cause else "NULL"}")
            }
        }
    }

    private fun isDev() = env.acceptsProfiles(of(SPRING_PROFILE_DEVELOPMENT))

    /**
     * Advice that logs when a method is entered and exited.
     *
     * @param joinPoint join point for advice.
     * @return result.
     * @throws Throwable throws [IllegalArgumentException].
     */
    @Around("applicationPackagePointcut() && springBeanPointcut()")
    @Throws(Throwable::class)
    fun logAround(joinPoint: ProceedingJoinPoint): Any = with(logger(joinPoint)) {
        if (isDebugEnabled) debug("Enter: ${joinPoint.signature.name}() with argument[s] = ${Arrays.toString(joinPoint.args)}")
        try {
            val result = joinPoint.proceed()
            if (isDebugEnabled) {
                debug("Exit: ${joinPoint.signature.name}() with result = $result")
            }
            result
        } catch (e: IllegalArgumentException) {
            error("Illegal argument: ${Arrays.toString(joinPoint.args)} in ${joinPoint.signature.name}()")
            throw e
        }
    }
}
