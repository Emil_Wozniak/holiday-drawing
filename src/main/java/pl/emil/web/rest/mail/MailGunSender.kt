package pl.emil.web.rest.mail

import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import org.apache.commons.codec.CharEncoding.UTF_8
import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerResponse.ok
import pl.emil.domain.User
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

@Component
class MailGunSender(
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    private val javaMailSender: JavaMailSender,
) {

    companion object {
        private const val SUBJECT = "emil.wozniak.591986@gmail.com"
        private const val IMG =
            "https://images.unsplash.com/photo-1510284876186-b1a84b94418f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80"
    }

    @Value("\${spring.mail.properties.mail.smtp.from}")
    lateinit var from: String

    @Value("\${mail.from.name}")
    lateinit var mailFrom: String

    @Async
    fun sendDrawnUserMail(user: User, drawUser: User) {
        with(javaMailSender) {
            send(createMail(user, drawUser))
        }
        ok().body(mapOf("message" to "Mail has been sent."))
    }

    private fun JavaMailSender.createMail(user: User, drawUser: User): MimeMessage =
        createMimeMessage().let { mimeMessage ->
            MimeMessageHelper(mimeMessage, true, UTF_8).apply {
                setTo(user.email!!)
                setSubject(SUBJECT)
                setFrom(InternetAddress(SUBJECT, mailFrom))
                setText(template(user, drawUser), true)
            }
            mimeMessage
        }

    fun template(user: User, drawUser: User): String = StringBuilder()
        .appendHTML()
        .html {
            head {
                title("Wylosowana osoba")
                meta(name = "Content-Type", content = "text/html", charset = "UTF-8")
            }
            body {
                drawnUserTemplate(user, drawUser)
            }
        }
        .toString()

    private fun BODY.drawnUserTemplate(user: User, drawUser: User) {
        div {
            style = "text-align:center;font-family: Helvetica, Sans-Serif;"
            picture {
                img(alt = "zdjęcie") {
                    src = IMG
                    style = "width:25%;height:25%"
                }
            }
            div {
                style = "text-align:center;color:#292727"
                h1 {
                    +"Witaj ${user.firstName} ${user.lastName}"
                }
                h2 {
                    +"Strona Holiday Drawing wylosowała dla Ciebie"
                }
                h2 {
                    +"${drawUser.firstName} ${drawUser.lastName}"
                }
            }
            div {
                style = "font-size:10px;" +
                    "color:gray;" +
                    "position:absolute;" +
                    "bottom:8px;" +
                    "right:16px;"
                p {
                    +"Wysłane przez Mailgun"
                }
                p {
                    +"Proszę nie odpowiadać na tą wiadomość"
                }
            }
        }
    }
}
