package pl.emil.web.rest

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.router
import pl.emil.web.rest.handler.*
import pl.emil.web.rest.mail.MailGunSender
import java.net.URI

@Configuration
class RouteConfig {

    @Bean
    fun routes(
        client: ClientForwardHandler,
        accountHandler: AccountHandler,
        userJWTHandler: UserJWTHandler,
        adminUserHandler: AdminUserHandler,
        userHandler: PublicUserHandler,
        entrantHandler: EntrantHandler,
        drawHandler: DrawHandler,
        mailGunSender: MailGunSender
    ) = router {
        "api".nest {
            POST("register", accountHandler::registerAccount)
            GET("activate", accountHandler::activateAccount)
            GET("authenticate", accountHandler::checkAuthenticated)
            POST("auth", userJWTHandler::authorize)
            GET("authorities", userHandler::getAuthorities)
            GET("users", userHandler::allPublicUsers)
            "admin".nest {
                "user".nest {
                    POST(adminUserHandler::createUser)
                    GET(adminUserHandler::getAllUsers)
                }
            }
            "account".nest {
                "change-password".nest {
                    POST(accountHandler::changePassword)
                }
                "reset-password".nest {
                    POST("init", accountHandler::requestPasswordReset)
                    POST("finish", accountHandler::finishPasswordReset)
                }
                GET(accountHandler::getAccount)
                POST(accountHandler::saveAccount)
            }
            "entrants".nest {
                "{id}".nest {
                    PUT(entrantHandler::updateEntrant)
                    PATCH(entrantHandler::partialUpdateEntrant)
                    GET(entrantHandler::getEntrant)
                    DELETE(entrantHandler::deleteEntrant)
                }
                "".nest {
                    PUT(entrantHandler::createEntrant)
                    GET(entrantHandler::allEntrants)
                }
            }
            "draw".nest {
                GET("all", drawHandler::didAllPlayersDrawn)
                GET("remain", drawHandler::remainingVotes)
                "{id}".nest {
                    GET("person", drawHandler::getDrawnPerson)
                    POST(drawHandler::draw)
                    GET(drawHandler::hasDrown)
                }
            }
        }
        GET("/") { ok().render("static/index.html") }
        GET("/account/**") {
            ServerResponse.temporaryRedirect(URI("/")).build()
        }
//        "/**/{path:[^.]*}"
//        resources("/**", ClassPathResource("static/index.html"))
    }
}
