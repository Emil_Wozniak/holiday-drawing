package pl.emil.web.rest.handler

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder.getContext
import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.ok
import org.springframework.web.servlet.function.body
import pl.emil.security.jwt.JWTFilter.AUTHORIZATION_HEADER
import pl.emil.security.jwt.TokenProvider
import pl.emil.web.rest.vm.LoginVM
import javax.validation.constraints.Size

/**
 * Controller to authenticate users.
 */
@Component
class UserJWTHandler(
    private val tokenProvider: TokenProvider,
    private val authManager: AuthenticationManagerBuilder,
) {
    fun authorize(request: ServerRequest): ServerResponse =
        request.body<LoginVM>().let { (username, password, isRememberMe) ->
            with(authManager.verify(username, password)) {
                getContext().authentication = this
                tokenProvider.createToken(this, isRememberMe).let { jwt ->
                    ok()
                        .header(AUTHORIZATION_HEADER, "Bearer $jwt")
                        .body(JWTToken(jwt))
                }
            }
        }

    private fun createToken(
        username: @Size(max = 50, min = 1) String,
        password: @Size(max = 100, min = 4) String,
    ) = UsernamePasswordAuthenticationToken(username, password)

    fun AuthenticationManagerBuilder.verify(username: String, password: String): Authentication =
        this.getObject().authenticate(createToken(username, password))

    /**
     * Object to return as body in JWT Authentication.
     */
    class JWTToken(@get:JsonProperty("id_token") var idToken: String)
}
