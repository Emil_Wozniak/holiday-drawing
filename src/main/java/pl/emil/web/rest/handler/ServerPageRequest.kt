package pl.emil.web.rest.handler


import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import java.util.function.Function
import java.util.function.Predicate

interface ServerPageRequest {
    companion object {
        private fun toPageable(request: ServerRequest): Pageable {
            val params = request.params()
            if (params.size > 2) {
                val pageValue = params.getFirst("page")
                val sizeValue = params.getFirst("size")
                val sortValue = params.getFirst("sort")
                if (pageValue != null && sizeValue != null && sortValue != null) {
                    val page = pageValue.toInt()
                    val size = sizeValue.toInt()
                    val sortable = sortablePage(sortValue, page, size)
                    return sortable ?: PageRequest.of(page, size)
                }
            }
            return Pageable.unpaged()
        }

        /**
         * experimental feature
         *
         * @param sortValue ServerRequest sort param value
         * @param page      page number
         * @param size      page size
         * @return Pageable
         */
        private fun sortablePage(sortValue: String, page: Int, size: Int): PageRequest? {
            if (isSortable(sortValue)) {
                val parameters = sortValue
                    .replace("asc", " ")
                    .replace("desc", " ")
                    .replace(", ".toRegex(), "")
                    .replace(" ,".toRegex(), "")
                val sort = if (isSortDirection.test(sortValue)) Sort.by(
                    if (isAscending.test(sortValue)) Sort.Direction.ASC else Sort.Direction.DESC,
                    parameters
                ) else Sort.by(*sortValue.split(",".toRegex()).toTypedArray())
                return PageRequest.of(page, size, sort)
            }
            return null
        }

        private fun isSortable(sortValue: String): Boolean {
            return sortValue.isNotEmpty() || sortValue.isNotBlank()
        }

        fun ServerRequest.asPageable(
            callback: Function<Pageable, ServerResponse>,
        ): ServerResponse =
            toPageable(this).let {
                if (it.isPaged) callback.apply(it)
                else callback.apply(it)// ServerResponse.badRequest().build()
            }

        private val isSortDirection =
            Predicate { sortValue: String ->
                sortValue.contains("asc") || sortValue.contains(
                    "desc"
                )
            }
        private val isAscending =
            Predicate { sortValue: String -> sortValue.contains("asc") }
    }
}

