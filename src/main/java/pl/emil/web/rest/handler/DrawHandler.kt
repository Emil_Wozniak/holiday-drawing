package pl.emil.web.rest.handler

import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.ok
import pl.emil.service.DrawService

@Component
class DrawHandler(
    private val drawService: DrawService,
) {
    fun draw(request: ServerRequest): ServerResponse =
        ok().body(drawService.draw(request.pathVariable("id").toLong()))

    fun hasDrown(request: ServerRequest): ServerResponse =
        ok().body(drawService.hasDrown(request.pathVariable("id").toLong()))

    fun didAllPlayersDrawn(request: ServerRequest): ServerResponse =
        ok().body(drawService.didAllPlayersDrawn())

    fun remainingVotes(request: ServerRequest): ServerResponse =
        ok().body(drawService.remainingVotes())

    fun getDrawnPerson(request: ServerRequest): ServerResponse =
        ok().body(drawService.getDrawnPerson(request.pathVariable("id").toLong()))
}
