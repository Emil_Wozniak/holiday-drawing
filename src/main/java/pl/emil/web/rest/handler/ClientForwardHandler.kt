package pl.emil.web.rest.handler

import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Service
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.ok

@Service
class ClientForwardHandler {
    fun render(request: ServerRequest): ServerResponse =
        ok().body(ClassPathResource("static/index.html"))

}
