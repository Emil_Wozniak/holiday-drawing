package pl.emil.web.rest.handler

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus.*
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.*
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.body
import org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest
import pl.emil.config.Constants.LOGIN_REGEX
import pl.emil.domain.User
import pl.emil.repository.UserRepository
import pl.emil.security.AuthoritiesConstants.ADMIN
import pl.emil.service.MailService
import pl.emil.service.UserService
import pl.emil.service.dto.AdminUserDTO
import pl.emil.web.rest.errors.BadRequestAlertException
import pl.emil.web.rest.errors.EmailAlreadyUsedException
import pl.emil.web.rest.errors.LoginAlreadyUsedException
import pl.emil.web.rest.handler.ServerPageRequest.Companion.asPageable
import tech.jhipster.web.util.HeaderUtil.createAlert
import tech.jhipster.web.util.PaginationUtil.generatePaginationHttpHeaders
import tech.jhipster.web.util.ResponseUtil.wrapOrNotFound
import java.net.URI
import java.net.URISyntaxException
import java.util.Locale.getDefault
import javax.validation.Valid
import javax.validation.constraints.Pattern

/**
 * REST controller for managing users.
 *
 *
 * This class accesses the [User] entity, and needs to fetch its collection of authorities.
 *
 *
 * For a normal use-case, it would be better to have an eager relationship between User and Authority,
 * and send everything to the client side: there would be no View Model and DTO, a lot less code, and an outer-join
 * which would be good for performance.
 *
 *
 * We use a View Model and a DTO for 3 reasons:
 *
 *  * We want to keep a lazy association between the user and the authorities, because people will
 * quite often do relationships with the user, and we don't want them to get the authorities all
 * the time for nothing (for performance reasons). This is the #1 goal: we should not impact our users'
 * application because of this use-case.
 *  *  Not having an outer join causes n+1 requests to the database. This is not a real issue as
 * we have by default a second-level cache. This means on the first HTTP call we do the n+1 requests,
 * but then all authorities come from the cache, so in fact it's much better than doing an outer join
 * (which will get lots of data from the database, for each HTTP call).
 *  *  As this manages users, for security reasons, we'd rather have a DTO layer.
 *
 *
 *
 * Another option would be to have a specific JPA entity graph to handle this case.
 */
@RestController
@RequestMapping("/api/admin")
class AdminUserHandler(
    private val userService: UserService,
    private val userRepository: UserRepository,
    private val mailService: MailService,
) {
    private val log = LoggerFactory.getLogger(AdminUserHandler::class.java)

    @Value("\${jhipster.clientApp.name}")
    private lateinit var applicationName: String

    @Throws(URISyntaxException::class)
    fun createUser(request: ServerRequest): ServerResponse {
        return request.body<AdminUserDTO>().let { userDTO ->
            log.debug("REST request to save User : {}", userDTO)
            when {
                userDTO.id != null -> throw BadRequestAlertException("A new user cannot already have an ID", "userManagement", "idexists")
                userRepository.findOneByLogin(userDTO.login.lowercase(getDefault())).isPresent ->
                    throw LoginAlreadyUsedException()
                userRepository.findOneByEmailIgnoreCase(userDTO.email).isPresent ->
                    throw EmailAlreadyUsedException()
                else -> userService.createUser(userDTO).let { user ->
                    mailService.sendCreationEmail(user)
                    ServerResponse.created(URI("/api/admin/users/${user.login}"))
                        .headers { createAlert(applicationName, "userManagement.created", user.login) }
                        .body(user)
                }
            }
        }
    }

    /**
     * `PUT /admin/users` : Updates an existing User.
     *
     * @param userDTO the user to update.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the updated user.
     * @throws EmailAlreadyUsedException `400 (Bad Request)` if the email is already in use.
     * @throws LoginAlreadyUsedException `400 (Bad Request)` if the login is already in use.
     */
    @PutMapping("/users")
    @PreAuthorize("hasAuthority(\"$ADMIN\")")
    fun updateUser(@RequestBody userDTO: @Valid AdminUserDTO): ResponseEntity<AdminUserDTO> {
        log.debug("REST request to update User : {}", userDTO)
        userRepository.findOneByEmailIgnoreCase(userDTO.email).let {
            when {
                it.isPresent && it.get().id != userDTO.id -> {
                    throw EmailAlreadyUsedException()
                }
                else -> {
                    userRepository.findOneByLogin(userDTO.login.lowercase(getDefault())).let { next ->
                        if (next.isPresent && next.get().id != userDTO.id) {
                            throw LoginAlreadyUsedException()
                        }
                        val updatedUser = userService.updateUser(userDTO)
                        return wrapOrNotFound(
                            updatedUser,
                            createAlert(applicationName, "userManagement.updated", userDTO.login)
                        )
                    }
                }
            }
        }
    }

    fun getAllUsers(request: ServerRequest): ServerResponse =
        request.asPageable { pageable ->
            log.debug("REST request to get all User for an admin")
            when {
                !onlyContainsAllowedProperties(pageable) -> ServerResponse.badRequest().build()
                else -> with(userService.getAllManagedUsers(pageable)) {
                    ServerResponse.ok().headers {
                        generatePaginationHttpHeaders(fromCurrentRequest(), this)
                    }.body(this.content)
                }
            }
        }

    private fun onlyContainsAllowedProperties(pageable: Pageable): Boolean =
        pageable.sort.map { it.property }.all { ALLOWED_ORDERED_PROPERTIES.contains(it) }

    /**
     * `GET /admin/users/:login` : get the "login" user.
     *
     * @param login the login of the user to find.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the "login" user, or with status `404 (Not Found)`.
     */
    @GetMapping("/users/{login}")
    @PreAuthorize("hasAuthority(\"$ADMIN\")")
    fun getUser(@PathVariable login: @Pattern(regexp = LOGIN_REGEX) String): ResponseEntity<AdminUserDTO> {
        log.debug("REST request to get User : $login")
        return userService.getUserWithAuthoritiesByLogin(login)
            .map { ok().body(it?.let { AdminUserDTO(it) }) }
            .orElseThrow { ResponseStatusException(NOT_FOUND) }
    }

    /**
     * `DELETE /admin/users/:login` : delete the "login" User.
     *
     * @param login the login of the user to delete.
     * @return the [ResponseEntity] with status `204 (NO_CONTENT)`.
     */
    @DeleteMapping("/users/{login}")
    @PreAuthorize("hasAuthority(\"$ADMIN\")")
    fun deleteUser(@PathVariable login: @Pattern(regexp = LOGIN_REGEX) String?): ResponseEntity<Void> {
        log.debug("REST request to delete User: {}", login)
        userService.deleteUser(login)
        return noContent().headers(createAlert(applicationName, "userManagement.deleted", login)).build()
    }

    companion object {
        private val ALLOWED_ORDERED_PROPERTIES = listOf(
            "id",
            "login",
            "firstName",
            "lastName",
            "email",
            "activated",
            "langKey",
            "createdBy",
            "createdDate",
            "lastModifiedBy",
            "lastModifiedDate"
        )
    }
}
