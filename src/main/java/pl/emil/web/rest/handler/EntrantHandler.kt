package pl.emil.web.rest.handler

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.*
import org.springframework.web.servlet.function.body
import pl.emil.domain.Entrant
import pl.emil.repository.EntrantRepository
import pl.emil.service.EntrantService
import pl.emil.web.rest.errors.BadRequestAlertException
import java.net.URI
import java.net.URISyntaxException

/**
 * Handler for managing [pl.emil.domain.Entrant].
 */
@Component
class EntrantHandler(
    private val entrantService: EntrantService,
    private val entrantRepository: EntrantRepository
) {
    private val log = LoggerFactory.getLogger(EntrantHandler::class.java)

    @Value("\${jhipster.clientApp.name}")
    private lateinit var applicationName: String


    /**
     * `POST  /entrants` : Create a new entrant.
     *
     * @param entrant the entrant to create.
     * @return the [ServerResponse] with status `201 (Created)` and with body the new entrant, or with status `400 (Bad Request)` if the entrant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @Throws(URISyntaxException::class)
    fun createEntrant(request: ServerRequest): ServerResponse {
        val entrant: Entrant = request.body()
        log.debug("REST request to save Entrant : {}", entrant)
        if (entrant.id != null) {
            throw BadRequestAlertException("A new entrant cannot already have an ID", ENTITY_NAME, "idexists")
        }
        val result = entrantService.save(entrant)
        return created(URI("/api/entrants/" + result.id)).body(result)
    }

    /**
     * `PUT  /entrants/:id` : Updates an existing entrant.
     *
     * @param id the id of the entrant to save.
     * @param entrant the entrant to update.
     * @return the [ServerResponse] with status `200 (OK)` and with body the updated entrant,
     * or with status `400 (Bad Request)` if the entrant is not valid,
     * or with status `500 (Internal Server Error)` if the entrant couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @Throws(URISyntaxException::class)
    fun updateEntrant(request: ServerRequest): ServerResponse {
        val id: Long = request.pathVariable("id").toLong()
        val entrant: Entrant = request.body()
        log.debug("REST request to update Entrant : $id, $entrant")
        verifyEntrant(entrant, id)
        val result = entrantService.save(entrant)
        return ok().body(result)
    }

    /**
     * `PATCH  /entrants/:id` : Partial updates given fields of an existing entrant, field will ignore if it is null
     *
     * @param id the id of the entrant to save.
     * @param entrant the entrant to update.
     * @return the [ServerResponse] with status `200 (OK)` and with body the updated entrant,
     * or with status `400 (Bad Request)` if the entrant is not valid,
     * or with status `404 (Not Found)` if the entrant is not found,
     * or with status `500 (Internal Server Error)` if the entrant couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @Throws(
        URISyntaxException::class
    )
    fun partialUpdateEntrant(request: ServerRequest): ServerResponse {
        val id: Long = request.pathVariable("id").toLong()
        val entrant: Entrant = request.body()
        log.debug("REST request to partial update Entrant partially : {}, {}", id, entrant)
        verifyEntrant(entrant, id)
        val result = entrantService.partialUpdate(entrant)
        return result.map { ok().body(it) }.orElse(notFound().build())
    }

    /**
     * `GET  /entrants` : get all the entrants.
     *
     * @return the [ServerResponse] with status `200 (OK)` and the list of entrants in body.
     */
    fun allEntrants(request: ServerRequest): ServerResponse {
        log.debug("REST request to get all Entrants")
        return ok().body(entrantService.findAll())
    }

    /**
     * `GET  /entrants/:id` : get the "id" entrant.
     *
     * @param id the id of the entrant to retrieve.
     * @return the [ServerResponse] with status `200 (OK)` and with body the entrant, or with status `404 (Not Found)`.
     */
    fun getEntrant(request: ServerRequest): ServerResponse {
        val id: Long = request.pathVariable("id").toLong()
        log.debug("REST request to get Entrant : $id")
        val entrant = entrantService.findOne(id)
        return entrant.map { ok().body(it) }.orElse(notFound().build())
    }

    private fun verifyEntrant(entrant: Entrant, id: Long) {
        if (entrant.id == null) {
            throw BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull")
        }
        if (id != entrant.id) {
            throw BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid")
        }
        if (!entrantRepository.existsById(id)) {
            throw BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound")
        }
    }

    /**
     * `DELETE  /entrants/:id` : delete the "id" entrant.
     *
     * @param id the id of the entrant to delete.
     * @return the [ServerResponse] with status `204 (NO_CONTENT)`.
     */
    fun deleteEntrant(request: ServerRequest): ServerResponse {
        val id: Long = request.pathVariable("id").toLong()
        log.debug("REST request to delete Entrant : $id")
        entrantService.delete(id)
        return noContent().build()
    }

    companion object {
        private const val ENTITY_NAME = "entrant"
    }
}
