package pl.emil.web.rest.handler

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.*
import org.springframework.web.servlet.function.body
import pl.emil.repository.UserRepository
import pl.emil.security.SecurityUtils.currentUserLogin
import pl.emil.service.MailService
import pl.emil.service.UserService
import pl.emil.service.dto.AdminUserDTO
import pl.emil.service.dto.PasswordChangeDTO
import pl.emil.web.rest.errors.EmailAlreadyUsedException
import pl.emil.web.rest.errors.InvalidPasswordException
import pl.emil.web.rest.vm.KeyAndPasswordVM
import pl.emil.web.rest.vm.ManagedUserVM
import pl.emil.web.rest.vm.ManagedUserVM.PASSWORD_MAX_LENGTH
import pl.emil.web.rest.vm.ManagedUserVM.PASSWORD_MIN_LENGTH
import java.net.URI

/**
 * REST handler for managing the current user's account.
 */
@Component
class AccountHandler(
    private val userRepository: UserRepository,
    private val userService: UserService,
    private val mailService: MailService,
) {
    private class AccountResourceException(message: String) : RuntimeException(message)

    private val log = LoggerFactory.getLogger(AccountHandler::class.java)

    fun registerAccount(request: ServerRequest): ServerResponse =
        request.body<ManagedUserVM>().let {
            if (isPasswordLengthInvalid(it.password)) {
                throw InvalidPasswordException()
            }
            userService.registerUser(it, it.password).let { user ->
                mailService.sendActivationEmail(user)
                created(URI.create("")).build()
            }
        }

    fun activateAccount(request: ServerRequest): ServerResponse =
        request.param("key")
            .map { key ->
                with(userService.activateRegistration(key)) {
                    if (isEmpty) throw AccountResourceException("No user was found for this activation key")
                }
                ok().build()
            }.orElseGet { badRequest().build() }

    fun checkAuthenticated(request: ServerRequest): ServerResponse =
        request.principal()
            .map { ok().body(it.name) }
            .orElseGet { ok().build() }

    fun getAccount(request: ServerRequest): ServerResponse =
        userService.getUserWithAuthorities()
            .map { ok().body(AdminUserDTO(it)) }
            .orElseGet { ok().build() }

    fun saveAccount(request: ServerRequest): ServerResponse =
        request.body<AdminUserDTO>().let { userDTO ->
            val userLogin = currentUserLogin.orElseThrow { AccountResourceException("Current user login not found") }
            val existingUser = userRepository.findOneByEmailIgnoreCase(userDTO.email)
            if (existingUser.isPresent && !existingUser.get().login.equals(userLogin, ignoreCase = true)) {
                throw EmailAlreadyUsedException()
            }
            val user = userRepository.findOneByLogin(userLogin)
            if (user.isEmpty) {
                throw AccountResourceException("User could not be found")
            }
            with(userDTO) {
                ok().body(userService.updateUser(firstName, lastName, email, langKey, imageUrl))
            }
        }

    fun changePassword(request: ServerRequest): ServerResponse =
        request.body<PasswordChangeDTO>().let {
            if (isPasswordLengthInvalid(it.newPassword)) {
                throw InvalidPasswordException()
            }
            ok().body(userService.changePassword(it.currentPassword, it.newPassword))
        }

    fun requestPasswordReset(request: ServerRequest): ServerResponse =
        request.body<String>().let { mail ->
            with(userService.requestPasswordReset(mail)) {
                if (isPresent) mailService.sendPasswordResetMail(get())
                else log.warn("Password reset requested for non existing mail")
            }
            ok().build()
        }

    fun finishPasswordReset(request: ServerRequest): ServerResponse =
        request.body<KeyAndPasswordVM>().let {
            if (isPasswordLengthInvalid(it.newPassword)) {
                throw InvalidPasswordException()
            }
            with(userService.completePasswordReset(it.newPassword, it.key)) {
                if (isEmpty) throw AccountResourceException("No user was found for this reset key")
            }
            ok().build()
        }

    companion object {
        private fun isPasswordLengthInvalid(password: String): Boolean =
            password.isEmpty() || password.length < PASSWORD_MIN_LENGTH || password.length > PASSWORD_MAX_LENGTH
    }
}
