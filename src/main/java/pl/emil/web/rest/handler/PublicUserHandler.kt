package pl.emil.web.rest.handler

import org.slf4j.LoggerFactory
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component
import org.springframework.web.servlet.function.ServerRequest
import org.springframework.web.servlet.function.ServerResponse
import org.springframework.web.servlet.function.ServerResponse.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequest
import pl.emil.service.UserService
import pl.emil.web.rest.utils.PaginationUtil.generatePaginationHttpHeaders

@Component
class PublicUserHandler(private val userService: UserService) {
    private val log = LoggerFactory.getLogger(PublicUserHandler::class.java)

    /**
     * `GET /users` : get all users with only the public informations - calling this are allowed for anyone.
     *
     * @param pageable the pagination information.
     * @return the [ServerResponse] with status `200 (OK)` and with body all users.
     */
    fun allPublicUsers(request: ServerRequest): ServerResponse =
        PageRequest.of(0, 20, Sort.by("id")).let { pageable ->
            log.debug("REST request to get all public User names")
            if (!onlyContainsAllowedProperties(pageable)) {
                badRequest().build()
            } else with(userService.getAllPublicUsers(pageable)) {
                ok()
                    .headers { generatePaginationHttpHeaders(fromCurrentRequest(), this)}
                    .body(content)
            }
        }

    /**
     * Gets a list of all roles.
     * @return a string list of all roles.
     */
    fun getAuthorities(request: ServerRequest): ServerResponse =
        ok().body(userService.getAuthorities())

    private fun onlyContainsAllowedProperties(pageable: Pageable): Boolean =
        pageable.sort
            .map { it.property }
            .all { ALLOWED_ORDERED_PROPERTIES.contains(it) }

    companion object {
        private val ALLOWED_ORDERED_PROPERTIES = listOf("id", "login", "firstName", "lastName", "email", "activated", "langKey")
    }
}
