/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.emil.web.rest.vm;
