package pl.emil.web.rest.vm

import javax.validation.constraints.Size

/**
 * View Model object for storing a user's credentials.
 */
data class LoginVM(
    var username: @Size(min = 1, max = 50) String = "",
    var password: @Size(min = 4, max = 100) String = "",
    var isRememberMe: Boolean = false,
) {
    override fun toString(): String = "LoginVM{" +
        "username='" + username + '\'' +
        ", rememberMe=" + isRememberMe +
        '}'
}
