package pl.emil.web.rest.errors

import pl.emil.web.rest.errors.ErrorConstants.EMAIL_ALREADY_USED_TYPE

class EmailAlreadyUsedException : BadRequestAlertException(EMAIL_ALREADY_USED_TYPE, "Email is already in use!", "userManagement", "emailexists") {
    companion object {
        private const val serialVersionUID = 1L
    }
}
