package pl.emil.web.rest.errors

class LoginAlreadyUsedException : BadRequestAlertException() {
    companion object {
        private const val serialVersionUID = 1L
    }
}
