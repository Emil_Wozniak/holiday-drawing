package pl.emil.web.rest.errors

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional

class InvalidPasswordException : AbstractThrowableProblem() {
    override fun getCause(): Exceptional {
        TODO("Not yet implemented")
    }

    companion object {
        const val serialVersionUID = 1L
    }
}
