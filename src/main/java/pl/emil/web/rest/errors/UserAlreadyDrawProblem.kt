package pl.emil.web.rest.errors

import org.zalando.problem.Exceptional
import pl.emil.web.rest.errors.ErrorConstants.USER_ALREADY_DRAW

class UserAlreadyDrawProblem: RuntimeException("Uzytkownik już głosował!")

class UserAlreadyDrawException : BadRequestAlertException(USER_ALREADY_DRAW, "Uzytkownik już głosował!","error.already_draw" ) {
    override fun getCause(): Exceptional {
        TODO("Not yet implemented")
    }
}
