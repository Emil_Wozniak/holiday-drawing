package pl.emil.web.rest.errors

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status.BAD_REQUEST
import pl.emil.web.rest.errors.ErrorConstants.DEFAULT_TYPE_URI
import java.net.URI

open class BadRequestAlertException(
    type: URI = DEFAULT_TYPE_URI,
    defaultMessage: String = "",
    val entityName: String = "",
    val errorKey: String = "") :
    AbstractThrowableProblem(type, defaultMessage, BAD_REQUEST, null, null, null, getAlertParameters(entityName, errorKey)) {

    constructor(defaultMessage: String, entityName: String, errorKey: String) : this(DEFAULT_TYPE_URI, defaultMessage, entityName, errorKey)

    companion object {
        private const val serialVersionUID = 1L
        private fun getAlertParameters(entityName: String, errorKey: String): Map<String, Any> {
            val parameters: MutableMap<String, Any> = HashMap()
            parameters["message"] = "error.$errorKey"
            parameters["params"] = entityName
            return parameters
        }
    }

    override fun getCause(): Exceptional {
        TODO("Not yet implemented")
    }
}
