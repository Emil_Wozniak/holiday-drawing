package pl.emil.web.rest.errors

import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.env.Environment
import org.springframework.dao.ConcurrencyFailureException
import org.springframework.dao.DataAccessException
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageConversionException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.NativeWebRequest
import org.zalando.problem.DefaultProblem
import org.zalando.problem.Problem
import org.zalando.problem.Problem.DEFAULT_TYPE
import org.zalando.problem.Problem.builder
import org.zalando.problem.ProblemBuilder
import org.zalando.problem.Status.CONFLICT
import org.zalando.problem.StatusType
import org.zalando.problem.spring.web.advice.ProblemHandling
import org.zalando.problem.spring.web.advice.security.SecurityAdviceTrait
import org.zalando.problem.violations.ConstraintViolationProblem
import pl.emil.service.EmailAlreadyUsedProblem
import pl.emil.service.InvalidPasswordProblem
import pl.emil.service.UsernameAlreadyUsedProblem
import pl.emil.web.rest.errors.ErrorConstants.CONSTRAINT_VIOLATION_TYPE
import pl.emil.web.rest.errors.ErrorConstants.DEFAULT_TYPE_URI
import pl.emil.web.rest.errors.ErrorConstants.ERR_CONCURRENCY_FAILURE
import pl.emil.web.rest.errors.ErrorConstants.ERR_VALIDATION
import tech.jhipster.config.JHipsterConstants.SPRING_PROFILE_PRODUCTION
import tech.jhipster.web.util.HeaderUtil.createFailureAlert
import java.net.URI
import java.util.Optional.ofNullable
import javax.annotation.Nonnull
import javax.servlet.http.HttpServletRequest

/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 * The error response follows RFC7807 - Problem Details for HTTP APIs (https://tools.ietf.org/html/rfc7807).
 */
@ControllerAdvice
class ExceptionTranslator(private val env: Environment) : ProblemHandling, SecurityAdviceTrait {
    @Value("\${jhipster.clientApp.name}")
    private val applicationName: String? = null

    /**
     * Post-process the Problem payload to add the message key for the front-end if needed.
     */
    override fun process(entity: ResponseEntity<Problem>?, request: NativeWebRequest): ResponseEntity<Problem>? {
        if (entity == null) {
            return null
        }
        val problem = entity.body
        if (!(problem is ConstraintViolationProblem || problem is DefaultProblem)) {
            return entity
        }
        val nativeRequest = request.getNativeRequest(HttpServletRequest::class.java)
        val requestUri = if (nativeRequest != null) nativeRequest.requestURI else StringUtils.EMPTY
        val builder = builder()
            .withType(if (DEFAULT_TYPE == problem.type) DEFAULT_TYPE_URI else problem.type)
            .withStatus(problem.status)
            .withTitle(problem.title)
            .with(PATH_KEY, requestUri)
        if (problem is ConstraintViolationProblem) {
            builder
                .with(VIOLATIONS_KEY, problem.violations)
                .with(MESSAGE_KEY, ERR_VALIDATION)
        } else {
            builder.withCause((problem as DefaultProblem).cause).withDetail(problem.getDetail())
                .withInstance(problem.getInstance())
            problem.getParameters().forEach { (key: String?, value: Any?) -> builder.with(key, value) }
            if (!problem.getParameters().containsKey(MESSAGE_KEY) && problem.getStatus() != null) {
                builder.with(MESSAGE_KEY, "error.http." + problem.getStatus()!!.statusCode)
            }
        }
        return ResponseEntity(builder.build(), entity.headers, entity.statusCode)
    }

    override fun handleMethodArgumentNotValid(
        ex: MethodArgumentNotValidException,
        @Nonnull request: NativeWebRequest,
    ): ResponseEntity<Problem> =
        create(ex, builder()
            .withType(CONSTRAINT_VIOLATION_TYPE)
            .withTitle("Method argument not valid")
            .withStatus(defaultConstraintViolationStatus())
            .with(MESSAGE_KEY, ERR_VALIDATION)
            .with(FIELD_ERRORS_KEY, ex.bindingResult.fieldErrors
                .map {
                    FieldErrorVM(
                        it.objectName.replaceFirst("DTO$".toRegex(), ""),
                        it.field,
                        if (it.defaultMessage?.isNotBlank() == true) it.defaultMessage!! else it.code
                    )
                })
            .build(), request)

    @ExceptionHandler
    fun handleEmailAlreadyUsedException(
        ex: EmailAlreadyUsedProblem,
        request: NativeWebRequest,
    ): ResponseEntity<Problem> = createMsg(EmailAlreadyUsedException(), request)

    @ExceptionHandler
    fun handleEmailAlreadyUsedException(ex: UserAlreadyDrawProblem, req: NativeWebRequest): ResponseEntity<Problem> =
        createMsg(UserAlreadyDrawException(), req)

    @ExceptionHandler
    fun handleUsernameAlreadyUsedException(
        ex: UsernameAlreadyUsedProblem,
        req: NativeWebRequest,
    ): ResponseEntity<Problem> =
        createMsg(LoginAlreadyUsedException(), req)

    @ExceptionHandler
    fun handleInvalidPasswordException(ex: InvalidPasswordProblem, request: NativeWebRequest): ResponseEntity<Problem> =
        create(InvalidPasswordException(), request)

    @ExceptionHandler
    fun handleBadRequestAlertException(ex: BadRequestAlertException, req: NativeWebRequest): ResponseEntity<Problem> =
        createMsg(ex, req)

    private fun createMsg(ex: BadRequestAlertException, request: NativeWebRequest) = with(ex) {
        create(this, request, createFailureAlert(applicationName, true, entityName, errorKey, message))
    }

    @ExceptionHandler
    fun handleConcurrencyFailure(
        ex: ConcurrencyFailureException,
        request: NativeWebRequest,
    ): ResponseEntity<Problem> = create(
        ex,
        builder()
            .withStatus(CONFLICT)
            .with(MESSAGE_KEY, ERR_CONCURRENCY_FAILURE)
            .build(),
        request)

    override fun prepare(throwable: Throwable, status: StatusType, type: URI): ProblemBuilder {
        val activeProfiles = listOf(*env.activeProfiles)
        if (activeProfiles.contains(SPRING_PROFILE_PRODUCTION)) {
            if (throwable is HttpMessageConversionException) {
                return builder()
                    .withType(type)
                    .withTitle(status.reasonPhrase)
                    .withStatus(status)
                    .withDetail("Unable to convert http message")
                    .withCause(
                        ofNullable(throwable.cause)
                            .filter { isCausalChainsEnabled }
                            .map { this.toProblem(it) }.orElse(null)
                    )
            }
            if (throwable is DataAccessException) {
                return builder()
                    .withType(type)
                    .withTitle(status.reasonPhrase)
                    .withStatus(status)
                    .withDetail("Failure during data access")
                    .withCause(
                        ofNullable(throwable.cause)
                            .filter { isCausalChainsEnabled }
                            .map { this.toProblem(it) }.orElse(null)
                    )
            }
            if (containsPackageName(throwable.message)) {
                return builder()
                    .withType(type)
                    .withTitle(status.reasonPhrase)
                    .withStatus(status)
                    .withDetail("Unexpected runtime exception")
                    .withCause(
                        ofNullable(throwable.cause)
                            .filter { isCausalChainsEnabled }
                            .map { this.toProblem(it) }.orElse(null)
                    )
            }
        }
        return builder()
            .withType(type)
            .withTitle(status.reasonPhrase)
            .withStatus(status)
            .withDetail(throwable.message)
            .withCause(
                ofNullable(throwable.cause)
                    .filter { isCausalChainsEnabled }
                    .map { this.toProblem(it) }.orElse(null)
            )
    }

    private fun containsPackageName(message: String?): Boolean {
        // This list is for sure not complete
        return StringUtils.containsAny(message, "org.", "java.", "net.", "javax.", "com.", "io.", "de.", "pl.emil")
    }

    companion object {
        private const val FIELD_ERRORS_KEY = "fieldErrors"
        private const val MESSAGE_KEY = "message"
        private const val PATH_KEY = "path"
        private const val VIOLATIONS_KEY = "violations"
    }
}
