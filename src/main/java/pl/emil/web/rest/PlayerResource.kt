package pl.emil.web.rest

import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.emil.domain.Player
import pl.emil.repository.PlayerRepository
import pl.emil.service.PlayerService
import tech.jhipster.web.util.ResponseUtil

/**
 * REST controller for managing [pl.emil.domain.Player].
 */
@RestController
@RequestMapping("/api")
class PlayerResource(
    private val playerService: PlayerService,
    private val playerRepository: PlayerRepository) {
    private val log = LoggerFactory.getLogger(PlayerResource::class.java)

    /**
     * `GET  /players` : get all the players.
     *
     * @return the [ResponseEntity] with status `200 (OK)` and the list of players in body.
     */
    @get:GetMapping("/players")
    val allPlayers: List<Player>
        get() {
            log.debug("REST request to get all Players")
            return playerService.findAll()
        }

    /**
     * `GET  /players/:id` : get the "id" player.
     *
     * @param id the id of the player to retrieve.
     * @return the [ResponseEntity] with status `200 (OK)` and with body the player, or with status `404 (Not Found)`.
     */
    @GetMapping("/players/{id}")
    fun getPlayer(@PathVariable id: Long?): ResponseEntity<Player> {
        log.debug("REST request to get Player : {}", id)
        val player = playerService.findOne(id!!)
        return ResponseUtil.wrapOrNotFound(player)
    }
}
