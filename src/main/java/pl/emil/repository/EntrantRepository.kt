package pl.emil.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import pl.emil.domain.Entrant
import java.util.*
import javax.validation.constraints.NotNull

/**
 * Spring Data SQL repository for the Entrant entity.
 */
@Repository
interface EntrantRepository : JpaRepository<Entrant, Long> {

    fun findByPlayerId(playerId: @NotNull Long): Optional<Entrant>

    @Query("select (count(e) > 0) from Entrant e where e.drawnPersonId = 0")
    fun didNotAllPlayersDrawn(): Boolean

    @Query("select count(e) from Entrant e where e.drawnPersonId = 0")
    fun remainingVotes(): Long

}
