package pl.emil.repository

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import pl.emil.domain.Player
import java.util.*
import javax.validation.constraints.NotNull

/**
 * Spring Data SQL repository for the Player entity.
 */
@Repository
interface PlayerRepository : JpaRepository<Player, Long> {

    fun findByPlayerId(playerId: @NotNull Long): Optional<Player?>
}
