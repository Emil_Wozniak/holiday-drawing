package pl.emil.repository

import org.springframework.data.jpa.repository.JpaRepository
import pl.emil.domain.Authority

/**
 * Spring Data JPA repository for the [Authority] entity.
 */
interface AuthorityRepository : JpaRepository<Authority?, String?>
