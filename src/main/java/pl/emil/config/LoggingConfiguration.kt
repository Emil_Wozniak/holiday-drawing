package pl.emil.config

import ch.qos.logback.classic.LoggerContext
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import tech.jhipster.config.JHipsterProperties
import tech.jhipster.config.logging.LoggingUtils.*

/*
 * Configures the console and Logstash log appenders from the app properties
 */
@Configuration
class LoggingConfiguration(
    @Value("\${spring.application.name}") appName: String,
    @Value("\${server.port}") serverPort: String,
    properties: JHipsterProperties,
    mapper: ObjectMapper,
) {
    init {
        val context = LoggerFactory.getILoggerFactory() as LoggerContext
        val customFields = mapper.writeValueAsString(
            hashMapOf(
                "app_name" to appName,
                "app_port" to serverPort
            ))
        val loggingProperties = properties.logging
        val logstashProperties = loggingProperties.logstash
        if (loggingProperties.isUseJsonFormat) addJsonConsoleAppender(context, customFields)
        if (logstashProperties.isEnabled) addLogstashTcpSocketAppender(context, customFields, logstashProperties)
        if (loggingProperties.isUseJsonFormat || logstashProperties.isEnabled) addContextListener(context, customFields, loggingProperties)
    }
}
