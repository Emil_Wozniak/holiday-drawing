package pl.emil.config

import org.springframework.cache.annotation.EnableCaching
import tech.jhipster.config.JHipsterProperties
import org.springframework.boot.info.GitProperties
import org.springframework.boot.info.BuildProperties
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer
import org.hibernate.cache.jcache.ConfigSettings
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer
import pl.emil.domain.Entrant
import org.springframework.beans.factory.annotation.Autowired
import tech.jhipster.config.cache.PrefixedKeyGenerator
import org.ehcache.jsr107.Eh107Configuration
import org.ehcache.config.builders.CacheConfigurationBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder
import org.ehcache.config.builders.ExpiryPolicyBuilder
import org.ehcache.config.builders.ExpiryPolicyBuilder.*
import org.ehcache.config.builders.ResourcePoolsBuilder.*
import org.hibernate.cache.jcache.ConfigSettings.*
import org.springframework.cache.interceptor.KeyGenerator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.emil.domain.Authority
import pl.emil.domain.User
import pl.emil.repository.UserRepository
import pl.emil.repository.UserRepository.Companion.USERS_BY_EMAIL_CACHE
import pl.emil.repository.UserRepository.Companion.USERS_BY_LOGIN_CACHE
import java.time.Duration
import java.time.Duration.*
import javax.cache.CacheManager

@Configuration
@EnableCaching
class CacheConfiguration(jHipsterProperties: JHipsterProperties) {
    private var gitProperties: GitProperties? = null
    private var buildProperties: BuildProperties? = null
    private val jcacheConfiguration: javax.cache.configuration.Configuration<Any, Any>
    @Bean
    fun hibernatePropertiesCustomizer(cacheManager: CacheManager): HibernatePropertiesCustomizer =
        HibernatePropertiesCustomizer { it[CACHE_MANAGER] = cacheManager }

    @Bean
    fun cacheManagerCustomizer(): JCacheManagerCustomizer {
        return JCacheManagerCustomizer { cm: CacheManager ->
            createCache(cm, USERS_BY_LOGIN_CACHE)
            createCache(cm, USERS_BY_EMAIL_CACHE)
            createCache(cm, User::class.java.name)
            createCache(cm, Authority::class.java.name)
            createCache(cm, User::class.java.name + ".authorities")
            createCache(cm, Entrant::class.java.name)
        }
    }

    private fun createCache(cm: CacheManager, cacheName: String) {
        val cache = cm.getCache<Any, Any>(cacheName)
        cache?.clear() ?: cm.createCache(cacheName, jcacheConfiguration)
    }

    @Autowired(required = false)
    fun setGitProperties(gitProperties: GitProperties?) {
        this.gitProperties = gitProperties
    }

    @Autowired(required = false)
    fun setBuildProperties(buildProperties: BuildProperties?) {
        this.buildProperties = buildProperties
    }

    @Bean
    fun keyGenerator(): KeyGenerator {
        return PrefixedKeyGenerator(gitProperties, buildProperties)
    }

    init {
        val ehcache = jHipsterProperties.cache.ehcache
        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder
                .newCacheConfigurationBuilder(Any::class.java, Any::class.java, heap(ehcache.maxEntries))
                .withExpiry(timeToLiveExpiration(ofSeconds(ehcache.timeToLiveSeconds.toLong())))
                .build()
        )
    }
}
