package pl.emil.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import tech.jhipster.config.JHipsterConstants
import pl.emil.aop.logging.LoggingAspect
import tech.jhipster.config.JHipsterConstants.*

@Configuration
@EnableAspectJAutoProxy
class LoggingAspectConfiguration {
    @Bean
    @Profile(SPRING_PROFILE_DEVELOPMENT)
    fun loggingAspect(env: Environment): LoggingAspect = LoggingAspect(env)
}
