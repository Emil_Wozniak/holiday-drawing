package pl.emil.config

import org.springframework.context.annotation.Configuration
import org.springframework.http.CacheControl
import org.springframework.http.CacheControl.*
import tech.jhipster.config.JHipsterProperties
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistration
import java.util.concurrent.TimeUnit.*

@Configuration
//@Profile({ SPRING_PROFILE_PRODUCTION })
class StaticResourcesWebConfiguration(private val properties: JHipsterProperties) : WebMvcConfigurer {
    override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
        initializeResourceHandler(appendResourceHandler(registry))
    }

    protected fun appendResourceHandler(registry: ResourceHandlerRegistry): ResourceHandlerRegistration =
        registry.addResourceHandler(*RESOURCE_PATHS)

    protected fun initializeResourceHandler(resourceHandlerRegistration: ResourceHandlerRegistration) {
        resourceHandlerRegistration
            .addResourceLocations(*RESOURCE_LOCATIONS)
            .setCacheControl(cacheControl)
    }

    protected val cacheControl: CacheControl
        get() = maxAge(cacheProperty.toLong(), DAYS).cachePublic()
    private val cacheProperty: Int
        get() = properties.http.cache.timeToLiveInDays

    companion object {
        protected val RESOURCE_LOCATIONS = arrayOf(
            "classpath:/static/",
            "classpath:/static/content/",
            "classpath:/static/i18n/")
        protected val RESOURCE_PATHS = arrayOf(
            "/*.js",
            "/*.css",
            "/*.svg",
            "/*.png",
            "*.ico",
            "/content/**",
            "/i18n/*")
    }
}
