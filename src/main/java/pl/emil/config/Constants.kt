package pl.emil.config

/**
 * Application constants.
 */
object Constants {
    // Regex for acceptable logins
    const val LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$"
    const val SYSTEM = "system"
    const val DEFAULT_LANGUAGE = "pl"
}
