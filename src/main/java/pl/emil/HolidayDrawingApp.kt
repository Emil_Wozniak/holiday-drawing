package pl.emil

import org.slf4j.LoggerFactory
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.core.env.Environment
import pl.emil.HolidayDrawingApp
import pl.emil.config.ApplicationProperties
import tech.jhipster.config.DefaultProfileUtil.addDefaultProfile
import tech.jhipster.config.JHipsterConstants.*
import java.net.InetAddress.getLocalHost
import java.net.UnknownHostException
import javax.annotation.PostConstruct

@SpringBootApplication
@EnableConfigurationProperties(LiquibaseProperties::class, ApplicationProperties::class)
class HolidayDrawingApp(private val env: Environment) {

    @PostConstruct
    fun initApplication() {
        listOf(*env.activeProfiles).let { profiles ->
            when {
                profiles.contains(SPRING_PROFILE_DEVELOPMENT) && profiles.contains(SPRING_PROFILE_PRODUCTION) ->
                    alert("prod")
                profiles.contains(SPRING_PROFILE_DEVELOPMENT) && profiles.contains(SPRING_PROFILE_CLOUD) ->
                    alert("cloud")
            }
        }
    }

    private fun alert(profile: String) {
        log.error("You have misconfigured your application! It should not run with both the 'dev' and '$profile' profiles at the same time.")
    }

    companion object {
        private val log = LoggerFactory.getLogger(HolidayDrawingApp::class.java)

        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication(HolidayDrawingApp::class.java).let {
                addDefaultProfile(it)
                it.run(*args).environment.logApplicationStartup()
            }
        }

        private fun Environment.logApplicationStartup() {
            val contextPath = getProperty("server.servlet.context-path").let { if (it == null || it.isBlank()) "/" else it  }
            val hostAddress: String = try {
                getLocalHost().hostAddress
            } catch (e: UnknownHostException) {
                log.warn("The host name could not be determined, using `localhost` as fallback")
                "localhost"
            }
            val profiles = if (activeProfiles.isEmpty()) defaultProfiles.toList() else activeProfiles.toList()
            val protocol = if (getProperty("server.ssl.key-store") != null) "https" else "http"
            log.info(
                """
----------------------------------------------------------
	Application '${getProperty("spring.application.name")}' is running! Access URLs:
	Local: 		$protocol://localhost:${getProperty("server.port")}${contextPath}
	External: 	$protocol://${hostAddress}:${getProperty("server.port")}${contextPath}
	Profile(s): 	$profiles
----------------------------------------------------------""",
            )
        }
    }
}
