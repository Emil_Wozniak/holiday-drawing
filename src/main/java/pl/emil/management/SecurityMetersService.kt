package pl.emil.management

import io.micrometer.core.instrument.Counter
import io.micrometer.core.instrument.MeterRegistry
import org.springframework.stereotype.Service
import pl.emil.management.SecurityMetersService

@Service
class SecurityMetersService(registry: MeterRegistry) {
    private val tokenInvalidSignatureCounter: Counter
    private val tokenExpiredCounter: Counter
    private val tokenUnsupportedCounter: Counter
    private val tokenMalformedCounter: Counter
    private fun invalidTokensCounterForCauseBuilder(cause: String): Counter.Builder = Counter
        .builder(INVALID_TOKENS_METER_NAME)
        .baseUnit(INVALID_TOKENS_METER_BASE_UNIT)
        .description(INVALID_TOKENS_METER_DESCRIPTION)
        .tag(INVALID_TOKENS_METER_CAUSE_DIMENSION, cause)

    fun trackTokenInvalidSignature() {
        tokenInvalidSignatureCounter.increment()
    }

    fun trackTokenExpired() {
        tokenExpiredCounter.increment()
    }

    fun trackTokenUnsupported() {
        tokenUnsupportedCounter.increment()
    }

    fun trackTokenMalformed() {
        tokenMalformedCounter.increment()
    }

    companion object {
        const val INVALID_TOKENS_METER_NAME = "security.authentication.invalid-tokens"
        const val INVALID_TOKENS_METER_DESCRIPTION = "Indicates validation error count of the tokens presented by the clients."
        const val INVALID_TOKENS_METER_BASE_UNIT = "errors"
        const val INVALID_TOKENS_METER_CAUSE_DIMENSION = "cause"
    }

    init {
        tokenInvalidSignatureCounter = invalidTokensCounterForCauseBuilder("invalid-signature").register(registry)
        tokenExpiredCounter = invalidTokensCounterForCauseBuilder("expired").register(registry)
        tokenUnsupportedCounter = invalidTokensCounterForCauseBuilder("unsupported").register(registry)
        tokenMalformedCounter = invalidTokensCounterForCauseBuilder("malformed").register(registry)
    }
}
