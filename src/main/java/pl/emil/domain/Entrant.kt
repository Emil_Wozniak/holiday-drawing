package pl.emil.domain

import org.hibernate.Hibernate
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE
import java.io.Serializable
import javax.persistence.*
import javax.persistence.GenerationType.SEQUENCE
import javax.validation.constraints.NotNull

/**
 * A Entrant.
 */
@Entity
@Table(name = "entrant")
@Cache(usage = READ_WRITE)
data class Entrant(
    @Id
    @SequenceGenerator(name = "sequenceGenerator")
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @Column(name = "id")
    var id: Long? = null,

    @Column(name = "player_id", nullable = false)
    var playerId: @NotNull Long? = null,

    @Column(name = "drawn_person_id")
    var drawnPersonId: Long? = null,

    @Column(name = "person_who_draw_participant_id")
    var personWhoDrawParticipantId: Long? = null
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Entrant

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(id = $id , playerId = $playerId , drawnPersonId = $drawnPersonId , personWhoDrawParticipantId = $personWhoDrawParticipantId )"
    }
}
