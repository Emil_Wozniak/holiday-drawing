package pl.emil.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.BatchSize
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE
import pl.emil.config.Constants.LOGIN_REGEX
import java.io.Serializable
import java.time.Instant
import javax.persistence.*
import javax.persistence.GenerationType.SEQUENCE
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

/**
 * A user.
 */
@Entity
@Table(name = "jhi_user")
@Cache(usage = NONSTRICT_READ_WRITE)
data class User(
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,

    @Size(min = 1, max = 50)
    @Pattern(regexp = LOGIN_REGEX)
    @Column(length = 50, unique = true, nullable = false)
    var login: String = "",

    @JsonIgnore
    @Column(name = "password_hash", length = 60, nullable = false)
    var password: @NotNull @Size(min = 60, max = 60) String? = null,

    @Column(name = "first_name", length = 50)
    var firstName: @Size(max = 50) String? = null,

    @Column(name = "last_name", length = 50)
    var lastName: @Size(max = 50) String? = null,

    @Column(length = 254, unique = true)
    var email: @Email @Size(min = 5, max = 254) String? = null,

    @Column(name = "activated", nullable = false)
    var activated: Boolean = false,

    @Column(name = "lang_key", length = 10)
    var langKey: @Size(min = 2, max = 10) String? = null,

    @Column(name = "image_url", length = 256)
    var imageUrl: @Size(max = 256) String? = null,

    @Column(name = "activation_key", length = 20)
    @JsonIgnore
    var activationKey: @Size(max = 20) String? = null,

    @Column(name = "reset_key", length = 20)
    @JsonIgnore
    var resetKey: @Size(max = 20) String? = null,

    @Column(name = "reset_date")
    var resetDate: Instant? = null,

    @JsonIgnore
    @ManyToMany
    @BatchSize(size = 20)
    @Cache(usage = NONSTRICT_READ_WRITE)
    @JoinTable(name = "jhi_user_authority",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "authority_name", referencedColumnName = "name")])
    var authorities: MutableSet<Authority> = mutableSetOf(),
) : AbstractAuditingEntity(), Serializable {

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }
        return if (other !is User) {
            false
        } else id != null && id == other.id
    }

    override fun hashCode(): Int {
        var result = login.hashCode()
        result = 31 * result + (password?.hashCode() ?: 0)
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (lastName?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + activated.hashCode()
        result = 31 * result + (langKey?.hashCode() ?: 0)
        result = 31 * result + (imageUrl?.hashCode() ?: 0)
        result = 31 * result + (activationKey?.hashCode() ?: 0)
        result = 31 * result + (resetKey?.hashCode() ?: 0)
        result = 31 * result + (resetDate?.hashCode() ?: 0)
        return result
    }


    companion object {
        const val serialVersionUID = 1L
    }
}
