package pl.emil.domain

import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.CacheConcurrencyStrategy.NONSTRICT_READ_WRITE
import java.io.Serializable
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/**
 * An authority (a security role) used by Spring Security.
 */
@Entity
@Table(name = "jhi_authority")
@Cache(usage = NONSTRICT_READ_WRITE)
class Authority(
    @Id
    @Column(length = 50)
    var name: @NotNull @Size(max = 50) String = ""
) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}
