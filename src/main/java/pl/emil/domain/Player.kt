package pl.emil.domain

import org.hibernate.Hibernate
import org.hibernate.annotations.Cache
import org.hibernate.annotations.CacheConcurrencyStrategy.READ_WRITE
import pl.emil.config.annotation.OpenClass
import java.io.Serializable
import javax.persistence.*
import javax.persistence.GenerationType.SEQUENCE
import javax.validation.constraints.NotNull

/**
 * A Player.
 */
@Entity
@OpenClass
@Table(name = "player")
@Cache(usage = READ_WRITE)
data class Player(
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    var id: Long? = null,

    @Column(name = "player_id", nullable = false, unique = true)
    var playerId: @NotNull Long? = null,

    @Lob
    @Column(name = "image")
    var image: ByteArray? = null,

    @Column(name = "image_content_type")
    var imageContentType: String? = null
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Player

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()

    @Override
    override fun toString(): String {
        return this::class.simpleName + "(playerId = $playerId , image = $image , imageContentType = $imageContentType )"
    }

}
