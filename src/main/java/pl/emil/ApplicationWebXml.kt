package pl.emil

import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer
import pl.emil.HolidayDrawingApp
import tech.jhipster.config.DefaultProfileUtil.addDefaultProfile

class ApplicationWebXml : SpringBootServletInitializer() {
    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        addDefaultProfile(application.application())
        return application.sources(HolidayDrawingApp::class.java)
    }
}
