package pl.emil.service.dto

class PasswordChangeDTO(
    val currentPassword: String,
    val newPassword: String,
)
