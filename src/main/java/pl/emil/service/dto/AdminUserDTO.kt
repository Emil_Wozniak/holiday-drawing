package pl.emil.service.dto

import pl.emil.config.Constants.LOGIN_REGEX
import pl.emil.config.annotation.OpenClass
import pl.emil.domain.User
import java.time.Instant
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

/**
 * A DTO representing a user, with his authorities.
 */
@OpenClass
data class AdminUserDTO(
    var id: Long? = null,
    var login: @NotBlank @Pattern(regexp = LOGIN_REGEX) @Size(min = 1, max = 50) String= "",
    var firstName: @Size(max = 50) String? = null,
    var lastName: @Size(max = 50) String? = null,
    var email: @Email @Size(min = 5, max = 254) String? = null,
    var imageUrl: @Size(max = 256) String? = null,
    var isActivated: Boolean = false,
    var langKey: @Size(min = 2, max = 10) String? = null,
    var createdBy: String? = null,
    var createdDate: Instant? = null,
    var lastModifiedBy: String? = null,
    var lastModifiedDate: Instant? = null,
    var authorities: Set<String>? = null,
) {

    constructor(user: User) : this() {
        id = user.id
        login = user.login
        firstName = user.firstName
        lastName = user.lastName
        email = user.email
        isActivated = user.activated
        imageUrl = user.imageUrl
        langKey = user.langKey
        createdBy = user.createdBy
        createdDate = user.createdDate
        lastModifiedBy = user.lastModifiedBy
        lastModifiedDate = user.lastModifiedDate
        authorities = user.authorities.map { it.name }.toSet()
    }
}
