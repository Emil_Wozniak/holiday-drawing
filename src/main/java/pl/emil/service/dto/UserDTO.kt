package pl.emil.service.dto

import pl.emil.domain.User

/**
 * A DTO representing a user, with only the public attributes.
 */
data class UserDTO(
    var id: Long? = null,
    var login: String? = null
) {

    constructor(user: User?): this() {
        id = user?.id
        login = user?.login
    }
}
