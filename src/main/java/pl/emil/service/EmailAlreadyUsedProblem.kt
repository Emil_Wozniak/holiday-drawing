package pl.emil.service

class EmailAlreadyUsedProblem : RuntimeException("Email is already in use!") {
    companion object {
        private const val serialVersionUID = 1L
    }
}
