package pl.emil.service

class UsernameAlreadyUsedProblem : RuntimeException("Login name already used!") {
    companion object {
        private const val serialVersionUID = 1L
    }
}
