package pl.emil.service

import org.springframework.stereotype.Service
import pl.emil.web.rest.errors.UserAlreadyDrawProblem
import pl.emil.web.rest.mail.MailGunSender

@Service
class DrawService(
    private val entrantService: EntrantService,
    private val userService: UserService,
    private val mailService: MailGunSender,
    private val playerService: PlayerService
) {
    fun draw(id: Long): DrawnPerson {
        val entrants = entrantService.findAll()
        val userEntrant = entrants.find { entrant -> entrant.playerId == id }
        var isNotMatching = true
        return when {
            userEntrant?.drawnPersonId != 0L -> throw UserAlreadyDrawProblem()
            else -> {
                entrants
                    .shuffled()
                    .filter { it.personWhoDrawParticipantId == 0L }
                    .distinct()
                    .forEach { entrant ->
                        if (isNotMatching && entrant.playerId != id && entrant.personWhoDrawParticipantId == 0L) {
                            isNotMatching = false
                            userEntrant.drawnPersonId = entrant.playerId
                            entrant.personWhoDrawParticipantId = userEntrant.playerId
                            entrantService.save(userEntrant)
                            entrantService.save(entrant)
                            mailService.sendDrawnUserMail(
                                user = userService.findById(userEntrant.playerId!!).get(),
                                drawUser = userService.findById(entrant.playerId!!).get()
                            )
                        }
                    }
                getUserByDrawnId(userEntrant.drawnPersonId!!)
            }
        }
    }

    fun hasDrown(id: Long): Boolean = entrantService.findByPlayerId(id).map { it.drawnPersonId != 0L }.orElse(false)

    fun getDrawnPerson(drawnPersonId: Long): DrawnPerson = entrantService.findByPlayerId(drawnPersonId)
        .map { getUserByDrawnId(it.drawnPersonId!!) }
        .orElseGet { DrawnPerson() }

    private fun getUserByDrawnId(drawnPersonId: Long) = userService.findById(drawnPersonId).get().let {
        playerService.findByPlayerId(drawnPersonId)
            .map { player ->
                DrawnPerson(drawnPersonId, it.firstName!!, it.lastName!!, player?.image, player?.imageContentType!!)
            }
            .orElseGet { DrawnPerson(drawnPersonId, it.firstName!!, it.lastName!!) }
    }

    fun didAllPlayersDrawn(): Boolean = entrantService.didAllPlayersDrawn()

    fun remainingVotes() = entrantService.remainingVotes()

    data class DrawnPerson(
        val id: Long = 0L,
        val firstName: String = "",
        val lastName: String = "",
        val image: ByteArray? = null,
        val imageType: String = ""
    )
}
