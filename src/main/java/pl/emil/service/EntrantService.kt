package pl.emil.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.emil.domain.Entrant
import pl.emil.repository.EntrantRepository
import java.time.LocalDateTime
import java.time.LocalDateTime.*
import java.util.*

/**
 * Service Implementation for managing [Entrant].
 */
@Service
@Transactional
class EntrantService(private val entrantRepository: EntrantRepository) {
    private val log = LoggerFactory.getLogger(EntrantService::class.java)

    /**
     * Save a entrant.
     *
     * @param entrant the entity to save.
     * @return the persisted entity.
     */
    fun save(entrant: Entrant): Entrant {
        log.debug("Request to save Entrant : {}", entrant)
        return entrantRepository.save(entrant)
    }

    /**
     * Partially update a entrant.
     *
     * @param entrant the entity to update partially.
     * @return the persisted entity.
     */
    fun partialUpdate(entrant: Entrant): Optional<Entrant> {
        log.debug("Request to partially update Entrant : $entrant")
        return entrantRepository.findById(entrant.id?: 0L).map {
            if (entrant.playerId != null) {
                it.playerId = entrant.playerId
            }
            if (entrant.drawnPersonId != null) {
                it.drawnPersonId = entrant.drawnPersonId
            }
            if (entrant.personWhoDrawParticipantId != null) {
                it.personWhoDrawParticipantId = entrant.personWhoDrawParticipantId
            }
            it
        }.map { entrantRepository.save(it) }
    }

    /**
     * Get all the entrants.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(): List<Entrant> {
        log.debug("Request to get all Entrants")
        return entrantRepository.findAll()
    }

    /**
     * Get one entrant by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<Entrant> {
        log.debug("Request to get Entrant : $id")
        return entrantRepository.findById(id)
    }

    /**
     * Delete the entrant by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Entrant : $id")
        entrantRepository.deleteById(id)
    }

    fun findByPlayerId(id: Long): Optional<Entrant> {
        return entrantRepository.findByPlayerId(id).map {
            log.info("User with id: $id drawn: ${it.drawnPersonId}")
            it
        }
    }

    fun didAllPlayersDrawn(): Boolean = !entrantRepository.didNotAllPlayersDrawn() && now().equals(of(2021,12, 19, 10,30))

    fun remainingVotes(): Long = entrantRepository.remainingVotes()
}
