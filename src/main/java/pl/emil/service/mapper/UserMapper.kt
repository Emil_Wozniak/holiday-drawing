package pl.emil.service.mapper

import org.mapstruct.BeanMapping
import org.mapstruct.Mapping
import org.mapstruct.Named
import org.springframework.stereotype.Service
import pl.emil.domain.Authority
import pl.emil.domain.User
import pl.emil.service.dto.AdminUserDTO
import pl.emil.service.dto.UserDTO
import java.util.*
import java.util.stream.Collectors

/**
 * Mapper for the entity [User] and its DTO called [UserDTO].
 *
 * Normal mappers are generated using MapStruct, this one is hand-coded as MapStruct
 * support is still in beta, and requires a manual step with an IDE.
 */
@Service
class UserMapper {
    fun usersToUserDTOs(users: List<User?>): List<UserDTO> {
        return users.filterNotNull().map { userToUserDTO(it) }.toMutableList()
    }

    fun userToUserDTO(user: User?): UserDTO = UserDTO(user!!)

    fun usersToAdminUserDTOs(users: List<User?>): List<AdminUserDTO> {
        return users.filterNotNull().map { userToAdminUserDTO(it) }.toMutableList()
    }

    fun userToAdminUserDTO(user: User?): AdminUserDTO = AdminUserDTO(user!!)

    fun userDTOsToUsers(userDTOs: List<AdminUserDTO?>): List<User?> {
        return userDTOs.filter { Objects.nonNull(it) }.map { userDTOToUser(it) }
    }

    fun userDTOToUser(userDTO: AdminUserDTO?): User? = if (userDTO == null) {
        null
    } else {
        User(
            id = userDTO.id,
            login = userDTO.login,
            firstName = userDTO.firstName,
            lastName = userDTO.lastName,
            email = userDTO.email,
            imageUrl = userDTO.imageUrl,
            activated = userDTO.isActivated,
            langKey = userDTO.langKey,
            authorities = authoritiesFromStrings(userDTO.authorities).toMutableSet()
        )
    }

    private fun authoritiesFromStrings(authoritiesAsString: Set<String>?): Set<Authority> {
        var authorities: Set<Authority> = mutableSetOf()
        if (authoritiesAsString != null) {
            authorities = authoritiesAsString
                .stream()
                .map {
                    val auth = Authority()
                    auth.name = it!!
                    auth
                }
                .collect(Collectors.toSet())
        }
        return authorities
    }

    fun userFromId(id: Long?): User? {
        if (id == null) {
            return null
        }
        val user = User()
        user.id = id
        return user
    }

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    fun toDtoId(user: User?): UserDTO? {
        if (user == null) {
            return null
        }
        val userDto = UserDTO()
        userDto.id = user.id
        return userDto
    }

    @Named("idSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    fun toDtoIdSet(users: Set<User?>?): Set<UserDTO?> {
        if (users == null) {
            return emptySet()
        }
        val userSet: MutableSet<UserDTO?> = HashSet()
        for (userEntity in users) {
            userSet.add(toDtoId(userEntity))
        }
        return userSet
    }

    @Named("login")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "login", source = "login")
    fun toDtoLogin(user: User?): UserDTO? {
        if (user == null) {
            return null
        }
        val userDto = UserDTO()
        userDto.id = user.id
        userDto.login = user.login
        return userDto
    }

    @Named("loginSet")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "login", source = "login")
    fun toDtoLoginSet(users: Set<User?>?): Set<UserDTO?> {
        if (users == null) {
            return emptySet()
        }
        val userSet: MutableSet<UserDTO?> = HashSet()
        for (userEntity in users) {
            userSet.add(toDtoLogin(userEntity))
        }
        return userSet
    }
}
