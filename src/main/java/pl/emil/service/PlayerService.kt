package pl.emil.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.emil.repository.PlayerRepository
import pl.emil.domain.Player
import java.util.*

/**
 * Service Implementation for managing [Player].
 */
@Service
@Transactional
class PlayerService(private val playerRepository: PlayerRepository) {
    private val log = LoggerFactory.getLogger(PlayerService::class.java)


    fun findByPlayerId(playerId: Long) = playerRepository.findByPlayerId(playerId)
    /**
     * Save a player.
     *
     * @param player the entity to save.
     * @return the persisted entity.
     */
    fun save(player: Player): Player {
        log.debug("Request to save Player : {}", player)
        return playerRepository.save(player)
    }

    /**
     * Partially update a player.
     *
     * @param player the entity to update partially.
     * @return the persisted entity.
     */
    fun partialUpdate(player: Player): Optional<Player> {
        log.debug("Request to partially update Player : {}", player)
        return playerRepository
            .findById(player.id)
            .map { existingPlayer: Player ->
                if (player.playerId != null) {
                    existingPlayer.playerId = player.playerId
                }
                if (player.image != null) {
                    existingPlayer.image = player.image
                }
                if (player.imageContentType != null) {
                    existingPlayer.imageContentType = player.imageContentType
                }
                existingPlayer
            }
            .map { entity: Player -> playerRepository.save(entity) }
    }

    /**
     * Get all the players.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    fun findAll(): List<Player> {
        log.debug("Request to get all Players")
        return playerRepository.findAll()
    }

    /**
     * Get one player by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    fun findOne(id: Long): Optional<Player> {
        log.debug("Request to get Player : {}", id)
        return playerRepository.findById(id)
    }

    /**
     * Delete the player by id.
     *
     * @param id the id of the entity.
     */
    fun delete(id: Long) {
        log.debug("Request to delete Player : {}", id)
        playerRepository.deleteById(id)
    }
}
