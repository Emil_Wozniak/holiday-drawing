package pl.emil.service

import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.mail.MailException
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine
import pl.emil.domain.User
import tech.jhipster.config.JHipsterProperties
import java.nio.charset.StandardCharsets.UTF_8
import java.util.Locale.forLanguageTag
import javax.mail.MessagingException

/**
 * Service for sending emails.
 *
 * We use the [Async] annotation to send emails asynchronously.
 */
@Service
class MailService(
    private val properties: JHipsterProperties,
    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    private val javaMailSender: JavaMailSender,
    private val messageSource: MessageSource,
    private val templateEngine: SpringTemplateEngine,
) {
    private val log = LoggerFactory.getLogger(MailService::class.java)

    @Async
    fun sendEmail(to: String, subject: String, content: String, isMultipart: Boolean, isHtml: Boolean) {
        log.debug("Send email[multipart '$isMultipart' and html '$isHtml'] to '$to' with subject '$subject' and content=$content")
        val mimeMessage = javaMailSender.createMimeMessage()
        try {
            MimeMessageHelper(mimeMessage, isMultipart, UTF_8.name()).apply {
                setTo(to)
                setFrom(properties.mail.from)
                setSubject(subject)
                setText(content, isHtml)
            }
            javaMailSender.send(mimeMessage)
            log.debug("Sent email to User '${to}'")
        } catch (e: MailException) {
            log.warn("Email could not be sent to user '$to'")
        } catch (e: MessagingException) {
            log.warn("Email could not be sent to user '${to}'")
        }
    }

    @Async
    fun sendEmailFromTemplate(user: User, templateName: String, titleKey: String) {
        if (user.email == null) {
            log.debug("Email doesn't exist for user '${user.login}'")
            return
        }
        val locale = forLanguageTag(user.langKey)
        sendEmail(
            user.email!!,
            messageSource.getMessage(titleKey, null, locale),
            templateEngine.process(
                templateName,
                Context(locale).apply {
                    setVariable(USER, user)
                    setVariable(BASE_URL, properties.mail.baseUrl)
                }
            ),
            isMultipart = false,
            isHtml = true
        )
    }

    @Async
    fun sendActivationEmail(user: User) {
        log.debug("Sending activation email to '${user.email}'")
        sendEmailFromTemplate(user, "activationEmail".template(), "email.activation.title")
    }

    @Async
    fun sendCreationEmail(user: User) {
        log.debug("Sending creation email to '${user.email}'")
        sendEmailFromTemplate(user, "creationEmail".template(), "email.activation.title")
    }

    @Async
    fun sendPasswordResetMail(user: User) {
        log.debug("Sending password reset email to '${user.email}'")
        sendEmailFromTemplate(user, "passwordResetEmail".template(), "email.reset.title")
    }

    companion object {
        private const val USER = "user"
        private const val BASE_URL = "baseUrl"
    }

    private fun String.template() = "mail/${this}"
}
