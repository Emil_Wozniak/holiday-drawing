package pl.emil.service

class InvalidPasswordProblem : RuntimeException() {
    companion object {
        const val serialVersionUID = 1L
    }
}
