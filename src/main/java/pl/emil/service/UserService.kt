package pl.emil.service

import org.slf4j.LoggerFactory
import org.springframework.cache.CacheManager
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.emil.config.Constants.DEFAULT_LANGUAGE
import pl.emil.domain.Authority
import pl.emil.domain.User
import pl.emil.repository.AuthorityRepository
import pl.emil.repository.UserRepository
import pl.emil.repository.UserRepository.Companion.USERS_BY_EMAIL_CACHE
import pl.emil.repository.UserRepository.Companion.USERS_BY_LOGIN_CACHE
import pl.emil.security.AuthoritiesConstants.USER
import pl.emil.security.SecurityUtils.currentUserLogin
import pl.emil.service.dto.AdminUserDTO
import pl.emil.service.dto.UserDTO
import tech.jhipster.security.RandomUtil.*
import java.time.Instant.now
import java.time.temporal.ChronoUnit.DAYS
import java.util.*
import java.util.Locale.getDefault
import java.util.Objects.requireNonNull

/**
 * Service class for managing users.
 */
@Service
@Transactional
class UserService(
    private val userRepository: UserRepository,
    private val passwordEncoder: PasswordEncoder,
    private val authorityRepository: AuthorityRepository,
    private val cacheManager: CacheManager,
) {
    private val log = LoggerFactory.getLogger(UserService::class.java)

    @Transactional(readOnly = true)
    fun findAllUsers(): List<User> {
        return userRepository.findAll()
    }

    fun findById(id: Long): Optional<User> = userRepository.findById(id)

    fun activateRegistration(key: String): Optional<User> {
        log.debug("Activating user for activation key {}", key)
        return userRepository
            .findOneByActivationKey(key)
            .map {
                it?.let {
                    it.activated = true
                    it.activationKey = null
                    clearUserCaches(it)
                    log.debug("Activated user: $it")
                    it
                }
            }
    }

    fun completePasswordReset(newPassword: String, key: String): Optional<User> {
        log.debug("Reset user password for reset key $key")
        return userRepository
            .findOneByResetKey(key)
            .filter { it?.resetDate!!.isAfter(now().minus(1, DAYS)) }
            .map {
                it?.let {
                    it.password = passwordEncoder.encode(newPassword)
                    it.resetKey = null
                    it.resetDate = null
                    clearUserCaches(it)
                    it
                }
            }
    }

    fun requestPasswordReset(mail: String): Optional<User> = userRepository
        .findOneByEmailIgnoreCase(mail)
        .filter { it?.activated ?: false }
        .map {
            it?.let {
                it.resetKey = generateResetKey()
                it.resetDate = now()
                clearUserCaches(it)
                it
            }
        }

    fun registerUser(userDTO: AdminUserDTO, password: String?): User {
        userRepository
            .findOneByLogin(userDTO.login.lowercase(getDefault()))
            .ifPresent {
                val removed = removeNonActivatedUser(it)
                if (!removed) {
                    throw UsernameAlreadyUsedProblem()
                }
            }
        userRepository
            .findOneByEmailIgnoreCase(userDTO.email)
            .ifPresent {
                val removed = removeNonActivatedUser(it)
                if (!removed) {
                    throw EmailAlreadyUsedProblem()
                }
            }
        val newUser = User()
        val encryptedPassword = passwordEncoder.encode(password)
        newUser.login = userDTO.login.lowercase(getDefault())
        // new user gets initially a generated password
        newUser.password = encryptedPassword
        newUser.firstName = userDTO.firstName
        newUser.lastName = userDTO.lastName
        if (userDTO.email != null) {
            newUser.email = userDTO.email?.lowercase(getDefault())
        }
        newUser.imageUrl = userDTO.imageUrl
        newUser.langKey = userDTO.langKey
        // new user is not active
        newUser.activated = false
        // new user gets registration key
        newUser.activationKey = generateActivationKey()
        val authorities: MutableSet<Authority> = HashSet()
        authorityRepository.findById(USER).ifPresent { authorities.add(it) }
        newUser.authorities = authorities
        userRepository.save(newUser)
        clearUserCaches(newUser)
        log.debug("Created Information for User: {}", newUser)
        return newUser
    }

    private fun removeNonActivatedUser(existingUser: User): Boolean {
        if (existingUser.activated) {
            return false
        }
        userRepository.delete(existingUser)
        userRepository.flush()
        clearUserCaches(existingUser)
        return true
    }

    fun createUser(userDTO: AdminUserDTO): User {
        val user = User()
        user.login = userDTO.login.lowercase(getDefault())
        user.firstName = userDTO.firstName
        user.lastName = userDTO.lastName
        if (userDTO.email != null) {
            user.email = userDTO.email?.lowercase(getDefault())
        }
        user.imageUrl = userDTO.imageUrl
        user.langKey = (if (userDTO.langKey == null) DEFAULT_LANGUAGE else userDTO.langKey)
        user.password = passwordEncoder.encode(generatePassword())
        user.resetKey = generateResetKey()
        user.resetDate = now()
        user.activated = true
        if (userDTO.authorities != null) {
            user.authorities = userDTO.authorities!!.map { authorityRepository.findById(it) }
                .filter { it.isPresent }
                .map { it.get() }
                .toMutableSet()
        }
        userRepository.save(user)
        clearUserCaches(user)
        log.debug("Created Information for User: $user")
        return user
    }

    /**
     * Update all information for a specific user, and return the modified user.
     *
     * @param userDTO user to update.
     * @return updated user.
     */
    fun updateUser(userDTO: AdminUserDTO): Optional<AdminUserDTO> {
        return Optional
            .of(userRepository.findById(userDTO.id ?: 0L))
            .filter { it.isPresent }
            .map { it.get() }
            .map { user ->
                clearUserCaches(user)
                with(user) {
                    login = userDTO.login.lowercase(getDefault())
                    firstName = userDTO.firstName
                    lastName = userDTO.lastName
                    if (userDTO.email != null) {
                        user.email = userDTO.email?.lowercase(getDefault())
                    }
                    imageUrl = userDTO.imageUrl
                    activated = userDTO.isActivated
                    langKey = userDTO.langKey
                }
                val managedAuthorities = user.authorities
                managedAuthorities.clear()
                userDTO
                    .authorities!!.map { authorityRepository.findById(it) }
                    .filter { it.isPresent }
                    .map { it.get() }
                    .forEach { managedAuthorities.add(it) }
                clearUserCaches(user)
                log.debug("Changed Information for User: $user")
                user
            }
            .map { AdminUserDTO(it) }
    }

    fun deleteUser(login: String?) {
        userRepository.findOneByLogin(login)
            .ifPresent { user: User ->
                userRepository.delete(user)
                clearUserCaches(user)
                log.debug("Deleted User: $user")
            }
    }

    /**
     * Update basic information (first name, last name, email, language) for the current user.
     *
     * @param firstName first name of user.
     * @param lastName  last name of user.
     * @param email     email id of user.
     * @param langKey   language key.
     * @param imageUrl  image URL of user.
     */
    fun updateUser(firstName: String?, lastName: String?, email: String?, langKey: String?, imageUrl: String?) {
        currentUserLogin
            .flatMap { userRepository.findOneByLogin(it) }
            .ifPresent {
                it.firstName = firstName
                it.lastName = lastName
                if (email != null) {
                    it.email = email.lowercase(getDefault())
                }
                it.langKey = langKey
                it.imageUrl = imageUrl
                clearUserCaches(it)
                log.debug("Changed Information for User: {}", it)
            }
    }

    @Transactional
    fun changePassword(currentClearTextPassword: String, newPassword: String) {
        currentUserLogin
            .flatMap { userRepository.findOneByLogin(it) }
            .ifPresent {
                if (!passwordEncoder.matches(currentClearTextPassword, it.password)) throw InvalidPasswordProblem()
                it.password = passwordEncoder.encode(newPassword)
                clearUserCaches(it)
                log.debug("Changed password for User: $it")
            }
    }

    @Transactional(readOnly = true)
    fun getAllManagedUsers(pageable: Pageable): Page<AdminUserDTO> {
        return userRepository.findAll(pageable).map { AdminUserDTO(it) }
    }

    @Transactional(readOnly = true)
    fun getAllPublicUsers(pageable: Pageable?): Page<UserDTO> =
        userRepository.findAllByIdNotNullAndActivatedIsTrue(pageable).map { UserDTO(it) }

    @Transactional(readOnly = true)
    fun getUserWithAuthoritiesByLogin(login: String): Optional<User?> {
        return userRepository.findOneWithAuthoritiesByLogin(login)
    }

    @Transactional(readOnly = true)
    fun getUserWithAuthorities(): Optional<User> =
        currentUserLogin.flatMap {
            userRepository.findOneWithAuthoritiesByLogin(it)
        }

    /**
     * Not activated users should be automatically deleted after 3 days.
     *
     *
     * This is scheduled to get fired everyday, at 01:00 (am).
     */
    @Scheduled(cron = "0 0 1 * * ?")
    fun removeNotActivatedUsers() {
        userRepository.findAllByActivatedIsFalseAndActivationKeyIsNotNullAndCreatedDateBefore(
                now().minus(3, DAYS)
            )
            .forEach {
                it?.let {
                    log.debug("Deleting not activated user ${it.login}")
                    userRepository.delete(it)
                    clearUserCaches(it)
                }
            }
    }

    /**
     * Gets a list of all the authorities.
     * @return a list of all the authorities.
     */
    @Transactional(readOnly = true)
    fun getAuthorities(): List<String?> = authorityRepository.findAll().map { it?.name }

    private fun clearUserCaches(user: User) {
        requireNonNull(cacheManager.getCache(USERS_BY_LOGIN_CACHE)).evict(user.login)
        if (user.email != null) {
            requireNonNull(cacheManager.getCache(USERS_BY_EMAIL_CACHE)).evict(user.email?: "")
        }
    }
}
