package pl.emil.security

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder.getContext
import org.springframework.security.core.userdetails.UserDetails
import pl.emil.security.AuthoritiesConstants.ANONYMOUS
import java.util.*
import java.util.stream.Stream

/**
 * Utility class for Spring Security.
 */
object SecurityUtils {
    /**
     * Get the login of the current user.
     *
     * @return the login of the current user.
     */
    @JvmStatic
    val currentUserLogin: Optional<String>
        get() {
            val securityContext = getContext()
            return Optional.ofNullable(extractPrincipal(securityContext.authentication))
        }

    private fun extractPrincipal(authentication: Authentication?): String? = when {
        authentication == null -> null
        authentication.principal is UserDetails -> (authentication.principal as UserDetails).username
        authentication.principal is String -> authentication.principal as String
        else -> null
    }

    /**
     * Get the JWT of the current user.
     *
     * @return the JWT of the current user.
     */
    val currentUserJWT: Optional<String?>
        get() {
            val securityContext = getContext()
            return Optional
                .ofNullable(securityContext.authentication)
                .filter { authentication: Authentication -> authentication.credentials is String }
                .map { authentication: Authentication -> authentication.credentials as String }
        }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise.
     */
    val isAuthenticated: Boolean
        get() = getContext().authentication.let { authentication ->
            authentication != null && getAuthorities(authentication).noneMatch { ANONYMOUS == it }
        }

    /**
     * Checks if the current user has any of the authorities.
     *
     * @param authorities the authorities to check.
     * @return true if the current user has any of the authorities, false otherwise.
     */
    fun hasCurrentUserAnyOfAuthorities(vararg authorities: String): Boolean {
        val authentication = getContext().authentication
        return authentication != null && getAuthorities(authentication).anyMatch { listOf(*authorities).contains(it) }
    }

    /**
     * Checks if the current user has none of the authorities.
     *
     * @param authorities the authorities to check.
     * @return true if the current user has none of the authorities, false otherwise.
     */
    fun hasCurrentUserNoneOfAuthorities(vararg authorities: String): Boolean = !hasCurrentUserAnyOfAuthorities(*authorities)

    /**
     * Checks if the current user has a specific authority.
     *
     * @param authority the authority to check.
     * @return true if the current user has the authority, false otherwise.
     */
    fun hasCurrentUserThisAuthority(authority: String): Boolean = hasCurrentUserAnyOfAuthorities(authority)

    private fun getAuthorities(authentication: Authentication): Stream<String> =
        authentication.authorities.stream().map { it.authority }
}
