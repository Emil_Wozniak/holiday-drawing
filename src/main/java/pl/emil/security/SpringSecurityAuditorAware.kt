package pl.emil.security

import pl.emil.security.SecurityUtils.currentUserLogin
import org.springframework.data.domain.AuditorAware
import org.springframework.stereotype.Component
import pl.emil.config.Constants
import pl.emil.config.Constants.SYSTEM
import pl.emil.security.SecurityUtils
import java.util.*

/**
 * Implementation of [AuditorAware] based on Spring Security.
 */
@Component
class SpringSecurityAuditorAware : AuditorAware<String> {
    override fun getCurrentAuditor(): Optional<String> = Optional.of(currentUserLogin.orElse(SYSTEM))
}
