package pl.emil.security

import org.hibernate.validator.internal.constraintvalidators.hv.EmailValidator
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pl.emil.domain.User
import pl.emil.repository.UserRepository
import java.util.Locale.ENGLISH
import org.springframework.security.core.userdetails.User as SpringUser

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
class DomainUserDetailsService(private val userRepository: UserRepository) : UserDetailsService {

    @Transactional
    override fun loadUserByUsername(login: String): UserDetails =
        when {
            EmailValidator().isValid(login, null) -> userRepository.findOneWithAuthoritiesByEmailIgnoreCase(login)
                .map { it?.let { createSpringSecurityUser(login, it) } }
                .orElseThrow { UsernameNotFoundException("User with email $login was not found in the database") }
            else -> login.lowercase(ENGLISH).let { lowercaseLogin ->
                userRepository
                    .findOneWithAuthoritiesByLogin(lowercaseLogin)
                    .map { it?.let { createSpringSecurityUser(lowercaseLogin, it) } }
                    .orElseThrow { UsernameNotFoundException("User $lowercaseLogin was not found in the database") }
            }
        }

    private fun createSpringSecurityUser(lowercaseLogin: String, user: User): SpringUser =
        when {
            !user.activated -> throw UserNotActivatedException("User $lowercaseLogin was not activated")
            else -> SpringUser(user.login, user.password, mapAuths(user))
        }

    private fun mapAuths(user: User) = user.authorities.map { SimpleGrantedAuthority(it.name) }
}
